(function() {
	$(document).ready(function(){

		// Dropdown Effect
		$(function() {
			$( '#cd-dropdown' ).dropdown();
		})

		// Image view library onclick
		$(function() {
			$('.gallery-grid a').Chocolat();
		});

		// Scroll effect library
		AOS.init();

		// Image slider library
		$('.owl-carousel').owlCarousel({
			loop:true,
			margin:0,
			nav:true,
			items:4,
			autoplay: true,
			autoplayTimeout: 2000,
			responsive : {
				// breakpoint from 0 up
				0 : {
					items:1
				},
				480 : {
					items:2
				},
				768 : {
					items:3
				},
				992 : {
					items:4
				}
			}
		});

		// Animate on page scroll
		$('.page-scroll').on('click', function(e){

			// Get href
			var target = $(this).attr('href')

			// Get Element
			var targetElement = $(target)

			// Move page
			$('html').animate({
				scrollTop: targetElement.offset().top - 60
			}, 1000, 'easeInOutExpo')

			e.preventDefault()

		})

	})
})();
