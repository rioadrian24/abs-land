<?php defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mailer {

	protected $_ci;
	protected $sender    = 'hello.absland@gmail.com';
	protected $name      = 'ABS LAND';
	protected $password  = 'absland6445';

	public function __construct()
	{
		$this->_ci = &get_instance();

		require_once(APPPATH . 'third_party/phpmailer/Exception.php');
		require_once(APPPATH . 'third_party/phpmailer/PHPMailer.php');
		require_once(APPPATH . 'third_party/phpmailer/SMTP.php');
	}

	public function send($data)
	{
		$mail = new PHPMailer;
		$mail->isSMTP();

		$mail->Host       = 'smtp.gmail.com';
		$mail->Username   = $this->sender;
		$mail->Password   = $this->password;
		$mail->Port       = 465;
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = 'ssl';
		// $mail->SMTPDebug  = 2;

		$mail->setFrom($this->sender, $this->name);
		$mail->addAddress($data['send_to'], '');
		$mail->isHTML(true);

		$mail->Subject          = 'Reply Your Message';
		$mail->Body             = $data['message'];
		$mail->AddEmbeddedImage('assets/images/logo/abs2.png', 'logo_absland', 'abs2.png');

		$send = $mail->send();

		if ($send) {
			$response = [
				'status'  => 'Success',
				'message' => 'Berhasil mengirim pesan!'
			];
		} else {
			$response = [
				'status'  => 'Failed',
				'message' => 'Gagal mengirim pesan!'
			];
		}

		return $response;
	}

}