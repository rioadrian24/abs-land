<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller
{
	public function index()
	{
		$data['projects'] = $this->db->get('project')->result_array();

		$this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('project/all', $data);
		$this->load->view('layout/footer');
	}

	public function view($id)
	{
		$data['project'] = $this->db->get_where('project', ['id_project' => $id])->row_array();
		$data['project_image'] = $this->db->get_where('project_image', ['id_project' => $id])->result_array();

		$this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('project/index', $data);
		$this->load->view('layout/footer');
	}

	public function search()
	{
		$keyword = $this->input->post(htmlspecialchars('keyword'));
		if (!is_null($keyword)) {
			$data['keyword'] = $keyword;
			$data['projects'] = $this->db->like('project_name', $keyword)->get('project')->result_array();
		} else {
			redirect('project');
		}

		$data['projects'] = $this->db->like('project_name', $keyword)->get('project')->result_array();

		$this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('project/result_search', $data);
		$this->load->view('layout/footer');
	}
	
}