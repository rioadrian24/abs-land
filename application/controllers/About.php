<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller
{

	public function index()
	{
		$data['about'] = $this->db->get('about')->row_array();
		$data['corporate_values'] = $this->db->get('corporate_value')->result_array();
		$data['visions'] = $this->db->get('vision')->result_array();
		$data['missions'] = $this->db->get('mission')->result_array();

		$this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('about/index', $data);
		$this->load->view('layout/footer');
	}
	
}