<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller
{

	public function index()
	{
		$data['news']     = $this->db->limit(6)->order_by('id_news', 'desc')->get_where('news', ['type' => null])->result_array();
		$data['feature'] = $this->db->limit(1)->order_by('id_news', 'desc')->get_where('news', ['type' => 'featured'])->result_array()[0];

		$this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('news/index', $data);
		$this->load->view('layout/footer');
	}
	
	public function allnews()
	{
		$data['news']     = $this->db->order_by('id_news', 'desc')->get_where('news', ['type' => null])->result_array();

		$this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('news/all', $data);
		$this->load->view('layout/footer');
	}

	public function read($id)
	{
		$data['news']      = $this->db->get_where('news', ['id_news' => $id])->row_array();
		$data['five_news'] = $this->db->limit(5)->get('news')->result_array();

		if (!$data['news']) {
			redirect('news');
		}

		$this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('news/read', $data);
		$this->load->view('layout/footer');
	}
}

?>