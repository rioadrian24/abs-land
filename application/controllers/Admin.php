<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{

	function __construct() {
		parent::__construct();

		is_admin();
	}

	public function index()
	{
		$data['page'] = 'dashboard';

		$this->load->view('admin/layout/header');
		$this->load->view('admin/layout/sidebar');
		$this->load->view('admin/dashboard/index', $data);
		$this->load->view('admin/layout/footer');
	}

	public function news()
	{
		$data['news'] = $this->db->order_by('id_news', 'desc')->get('news')->result_array();

		$this->load->view('admin/layout/header', $data);
		$this->load->view('admin/layout/sidebar', $data);
		$this->load->view('admin/layout/topbar', $data);
		$this->load->view('admin/news/index', $data);
		$this->load->view('admin/layout/footer', $data);
	}

	public function add_news()
	{
		$this->form_validation->set_rules('title', 'Title', 'required|trim', [
			'required' => 'Title is required!'
		]);
		$this->form_validation->set_rules('content', 'Content', 'required|trim', [
			'required' => 'Content is required!'
		]);

		if (empty($_FILES['thumbnail']['name']))
		{
			$this->form_validation->set_rules('thumbnail', 'Image', 'required', [
				'required' => 'Thumbnail is required!'
			]);
		}


		if ($this->form_validation->run() == false) {

			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/news/add');
			$this->load->view('admin/layout/footer');

		} else {

			$title = $this->input->post(htmlspecialchars('title'));
			$content = $this->input->post(htmlspecialchars('content'));

			$image = $_FILES['thumbnail'];

			$config['upload_path']   = './assets/images/news/thumbnail';
			$config['allowed_types'] = 'jpg|png';
			$config['max_size']      = 20000;

			$upload = $this->load->library('upload', $config);

			if (!$this->upload->do_upload('thumbnail')) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">
					<i class="fas fa-times-circle"></i> An error occurred while uploading the image!<br>
					<i class="fas fa-info-circle"></i> Please check your internet connection<br>
					<i class="fas fa-info-circle"></i> Please check your image size, Max size 20MB<br>
					<i class="fas fa-info-circle"></i> Please check your extension, must be JPG or PNG</div>');

				redirect('admin/add_news/');
			} else {
				$thumbnail = $this->upload->data('file_name');

				$data = [
					'title'     => $title,
					'content'   => $content,
					'thumbnail' => $thumbnail,
					'date'      => time()
				];

				$this->db->insert('news', $data);

				$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> News added successfully!</div>');

				redirect('admin/news');
			}
		}
	}

	function sn_upload_image(){
		if(isset($_FILES["image"]["name"])){
			$config['upload_path'] = './assets/images/news/content/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('image')){
				$this->upload->display_errors();
				return FALSE;
			} else {
				$data = $this->upload->data();
//Compress Image
				$config['image_library']='gd2';
				$config['source_image']='./assets/images/news/content/'.$data['file_name'];
				$config['create_thumb']= FALSE;
				$config['maintain_ratio']= TRUE;
				$config['quality']= '90%';
				$config['width']= 800;
				$config['height']= 800;
				$config['new_image']= './assets/images/news/content/'.$data['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				echo base_url() . 'assets/images/news/content/'.$data['file_name'];
			}
		}
	}

	function sn_delete_image(){
		$src = $this->input->post('src');
		$file_name = str_replace(base_url(), '', $src);
		if(unlink($file_name)){
			echo 'File Delete Successfully';
		}
	}

	public function edit_news($id)
	{
		$this->form_validation->set_rules('title', 'Title', 'required|trim', [
			'required' => 'Title is required!'
		]);
		$this->form_validation->set_rules('content', 'Content', 'required|trim', [
			'required' => 'Content is required!'
		]);

		if ($this->form_validation->run() == false) {
			$data['new'] = $this->db->get_where('news', ['id_news' => $id])->row_array();

			$this->load->view('admin/layout/header', $data);
			$this->load->view('admin/layout/sidebar', $data);
			$this->load->view('admin/layout/topbar', $data);
			$this->load->view('admin/news/edit', $data);
			$this->load->view('admin/layout/footer', $data);
		} else {
			if (empty($_FILES['thumbnail']['name'])) {
				$title = $this->input->post(htmlspecialchars('title'));
				$content = $this->input->post(htmlspecialchars('content'));

				$get_news = $this->db->get_where('news', ['id_news' => $id])->row_array();

				if ($get_news['title'] == $title AND $get_news['content'] == $content) {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">
						<i class="fas fa-times-circle"></i> An error occurred while uploading the image!<br>
						<i class="fas fa-info-circle"></i> Please check your internet connection<br>
						<i class="fas fa-info-circle"></i> Please check your image size, Max size 20MB<br>
						<i class="fas fa-info-circle"></i> Please check your extension, must be JPG or PNG</div>');

					redirect('admin/edit_news/' . $id);
				}

				$this->db->set('title', $title);
				$this->db->set('content', $content);
				$this->db->set('date_update', time());
				$this->db->where('id_news', $id);
				$this->db->update('news');

				$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> News edited successfully!</div>');

				redirect('admin/news');
			} else {
				$image = $_FILES['thumbnail'];

				$config['upload_path']   = './assets/images/news/thumbnail';
				$config['allowed_types'] = 'jpg|png';
				$config['max_size']      = 20000;

				$upload = $this->load->library('upload', $config);

				if (!$this->upload->do_upload('thumbnail')) {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">
						<i class="fas fa-times-circle"></i> An error occurred while uploading the image!<br>
						<i class="fas fa-info-circle"></i> Please check your internet connection<br>
						<i class="fas fa-info-circle"></i> Please check your image size, Max size 20MB<br>
						<i class="fas fa-info-circle"></i> Please check your extension, must be JPG or PNG</div>');

					redirect('admin/edit_news/' . $id);
				} else {
					$old_image = $this->db->get_where('news', ['id_news' => $id])->row_array();
					unlink('./assets/images/news/thumbnail/' . $old_image['thumbnail']);

					$title     = $this->input->post(htmlspecialchars('title'));
					$content   = $this->input->post(htmlspecialchars('content'));
					$thumbnail = $this->upload->data('file_name');

					$this->db->set('title', $title);
					$this->db->set('content', $content);
					$this->db->set('thumbnail', $thumbnail);
					$this->db->set('date_update', time());
					$this->db->where('id_news', $id);
					$this->db->update('news');


					$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> News edited successfully!</div>');

					redirect('admin/news');
				}

			}

		}
	}

	public function delete_news($id)
	{
		$old_image = $this->db->get_where('news', ['id_news' => $id])->row_array();
		unlink('./assets/images/news/thumbnail/' . $old_image['thumbnail']);

		$this->db->where('id_news', $id);
		$this->db->delete('news');

		$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> News deleted successfully!</div>');

		redirect('admin/news');
	}

	public function preview_news()
	{
		$id = $this->input->post(htmlspecialchars('id'));

		$get_news = $this->db->get_where('news', ['id_news' => $id])->row_array();

		if ($get_news) {
			echo json_encode($get_news);
		}
	}

	public function project()
	{
		$data['project'] = $this->db->order_by('id_project', 'desc')->get('project')->result_array();

		$this->load->view('admin/layout/header');
		$this->load->view('admin/layout/sidebar');
		$this->load->view('admin/layout/topbar');
		$this->load->view('admin/project/index', $data);
		$this->load->view('admin/layout/footer');
	}

	public function add_project()
	{
		$this->form_validation->set_rules('project_name', 'Project name', 'required|trim|is_unique[project.project_name]', [
			'required'  => 'Project name is required!',
			'is_unique' => 'Project name already exists!'
		]);

		$this->form_validation->set_rules('description', 'Description', 'required|trim', [
			'required'  => 'Description is required!'
		]);

		$this->form_validation->set_rules('maps', 'Maps', 'required|trim', [
			'required'  => 'Google maps link is required!'
		]);


		if ($this->form_validation->run() == false) {
			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/project/add');
			$this->load->view('admin/layout/footer');
		} else {
			$project_name = $this->input->post(htmlspecialchars('project_name'));
			$description  = $this->input->post(htmlspecialchars('description'));
			$maps         = $this->input->post(htmlspecialchars('maps'));
			$banner       = null;

			if (empty($_FILES['banner']['name'])) {
				$data = [
					'project_name'        => $project_name,
					'project_description' => $description,
					'gmaps_link'          => $maps,
					'banner'              => $banner
				];

				$this->db->insert('project', $data);

				$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Project has been added!</div>');

				redirect('admin/project');
			} else {
				$config['upload_path']   = './assets/images/projects/';
				$config['allowed_types'] = 'jpg|png|svg';
				$config['max_size']      = 10000;

				$upload = $this->load->library('upload', $config);

				if (!$this->upload->do_upload('banner')) {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">
						<i class="fas fa-times-circle"></i> An error occurred while uploading the image!<br>
						<i class="fas fa-info-circle"></i> Please check your internet connection<br>
						<i class="fas fa-info-circle"></i> Please check your image size, Max size 20MB<br>
						<i class="fas fa-info-circle"></i> Please check your extension, must be JPG or PNG</div>');

					redirect('admin/add_project');
				} else {
					$banner_name = $this->upload->data('file_name');

					$data = [
						'project_name'        => $project_name,
						'project_description' => $description,
						'gmaps_link'          => $maps,
						'banner'              => $banner_name
					];

					$this->db->insert('project', $data);
					$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Project added successfully!</div>');

					redirect('admin/project');
				}
			}
		}
	}

	public function view_project($id)
	{
		$this->form_validation->set_rules('image', 'Image', 'trim');

		if (empty($_FILES['image']['name'])) {
			$this->form_validation->set_rules('image', 'Gambar', 'required|trim', [
				'required' => 'Image is required!'
			]);
		}

		if ($this->form_validation->run() == false) {
			$data['project'] = $this->db->get_where('project', ['id_project' => $id])->row_array();
			$data['project_image'] = $this->db->get_where('project_image', ['id_project' => $id])->result_array();

			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/project/view', $data);
			$this->load->view('admin/layout/footer');
		} else {
			$config['upload_path']   = './assets/images/projects/';
			$config['allowed_types'] = 'jpg|png|svg';
			$config['max_size']      = 10000;

			$upload = $this->load->library('upload', $config);

			if (!$this->upload->do_upload('image')) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">
					<i class="fas fa-times-circle"></i> An error occurred while uploading the image!<br>
					<i class="fas fa-info-circle"></i> Please check your internet connection<br>
					<i class="fas fa-info-circle"></i> Please check your image size, Max size 20MB<br>
					<i class="fas fa-info-circle"></i> Please check your extension, must be JPG or PNG</div>');

				redirect('admin/view_project/' . $id);
			} else {
				$image_name = $this->upload->data('file_name');

				$data = [
					'id_project'   => $id,
					'image_name'   => $image_name
				];

				$this->db->insert('project_image', $data);

				$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Project image added successfully!</div>');

				redirect('admin/view_project/' . $id);
			}
		}
	}

	public function edit_project($id)
	{
		$this->form_validation->set_rules('project_name', 'Project Name', 'required|trim', [
			'required' => 'Project name is required!'
		]);

		if ($this->form_validation->run() == false) {
			$data['project'] = $this->db->get_where('project', ['id_project' => $id])->row_array();

			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/project/edit', $data);
			$this->load->view('admin/layout/footer');
		} else {
			$project_name  = $this->input->post(htmlspecialchars('project_name'));
			$description   = $this->input->post(htmlspecialchars('description'));
			$maps          = $this->input->post(htmlspecialchars('maps'));
			$default_image = $this->input->post(htmlspecialchars('default_image'));

			if ($default_image == "on") {
				$old_image = $this->db->get_where('project', ['id_project' => $id])->row_array();

				if ($old_image['banner'] != null) {
					unlink('./assets/images/projects/' . $old_image['banner']);
				}

				$this->db->set('banner', null);
				$this->db->set('project_name', $project_name);
				$this->db->set('project_description', $description);
				$this->db->set('maps', $maps);
				$this->db->where('id_project', $id);
				$this->db->update('project');

				$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Project edited successfully!</div>');

				redirect('admin/project');
			}

			if (empty($_FILES['banner']['name'])) {
				$this->db->set('project_name', $project_name);
				$this->db->where('id_project', $id);
				$this->db->update('project');

				$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Project edited successfully!</div>');

				redirect('admin/project');
			} else {
				$old_image = $this->db->get_where('project', ['id_project' => $id])->row_array();

				if ($old_image['banner'] != null) {
					unlink('./assets/images/projects/' . $old_image['banner']);
				}

				$config['upload_path']   = './assets/images/projects/';
				$config['allowed_types'] = 'jpg|png|svg';
				$config['max_size']      = 10000;

				$upload = $this->load->library('upload', $config);

				if (!$this->upload->do_upload('banner')) {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">
						<i class="fas fa-times-circle"></i> An error occurred while uploading the image!<br>
						<i class="fas fa-info-circle"></i> Please check your internet connection<br>
						<i class="fas fa-info-circle"></i> Please check your image size, Max size 20MB<br>
						<i class="fas fa-info-circle"></i> Please check your extension, must be JPG or PNG</div>');

					redirect('admin/edit_project');
				} else {
					$banner_name = $this->upload->data('file_name');

					$this->db->set('project_name', $project_name);
					$this->db->set('banner', $banner_name);
					$this->db->where('id_project', $id);
					$this->db->update('project');

					$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Project edited successfully!</div>');

					redirect('admin/project');
				}
			}
		}
	}

	public function delete_project($id)
	{
		$old_image = $this->db->get_where('project', ['id_project' => $id])->row_array();
		if ($old_image['banner'] != null) {
			unlink('./assets/images/projects/' . $old_image['banner']);
		}

		$this->db->where('id_project', $id);
		$this->db->delete('project');

		$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Project deleted successfully!</div>');

		redirect('admin/project');
	}

	public function delete_project_image($id)
	{
		$explode_id = explode('and', $id);

		$old_image = $this->db->get_where('project_image', ['id_project_image' => $explode_id[0]])->row_array();
		unlink('./assets/images/projects/' . $old_image['image_name']);

		$this->db->where('id_project_image', $explode_id[0]);
		$this->db->delete('project_image');

		$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Project image deleted successfully!</div>');

		redirect('admin/view_project/' . $explode_id[1]);
	}

	public function profile()
	{
		$data['admin'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();

		$this->load->view('admin/layout/header');
		$this->load->view('admin/layout/sidebar');
		$this->load->view('admin/layout/topbar');
		$this->load->view('admin/profile/index', $data);
		$this->load->view('admin/layout/footer');
	}

	public function change_picture() {
		$this->form_validation->set_rules('image', 'Image', 'trim');

		if (empty($_FILES['image']['name']) AND $this->input->post('default_image') != "on")
		{
			$this->form_validation->set_rules('image', 'Image', 'required', [
				'required' => 'Image is required!'
			]);
		}

		if ($this->form_validation->run() == false) {

			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/profile/change_picture');
			$this->load->view('admin/layout/footer');

		} else {

			if ($this->input->post('default_image') == "on") {
				$old_image = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
				if ($old_image['image'] != null) {
					unlink('./assets/images/users/' . $old_image['image']);
				}

				$this->db->set('image', null);
				$this->db->where('id_admin', $this->session->userdata('id_admin'));
				$this->db->update('admin');

				$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Change default image successfully!</div>');

				redirect('admin/profile');
			}

			$old_image = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
			if ($old_image['image'] != null) {
				unlink('./assets/images/users/' . $old_image['image']);
			}

			$config['upload_path']   = './assets/images/users';
			$config['allowed_types'] = 'jpg|png|svg|ico';
			$config['max_size']      = 10000;

			$upload = $this->load->library('upload', $config);

			if (!$this->upload->do_upload('image')) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">
					<i class="fas fa-times-circle"></i> An error occurred while uploading the image!<br>
					<i class="fas fa-info-circle"></i> Please check your internet connection<br>
					<i class="fas fa-info-circle"></i> Please check your image size, Max size 20MB<br>
					<i class="fas fa-info-circle"></i> Please check your extension, must be JPG or PNG</div>');

				redirect('admin/profile');
			} else {
				$this->db->set('image', $this->upload->data('file_name'));
				$this->db->set('date_update', time());
				$this->db->where('id_admin', $this->session->userdata('id_admin'));
				$this->db->update('admin');

				$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Picture edited successfully!</div>');

				redirect('admin/profile');
			}

		}
	}

	public function change_password()
	{
		$this->form_validation->set_rules('old_password', 'Old password', 'required|trim', [
			'required' => 'Old password is required!'
		]);
		$this->form_validation->set_rules('new_password', 'New password', 'required|trim|matches[re_password]|min_length[5]', [
			'required'   => 'New password is required!',
			'matches'    => 'Password not match!',
			'min_length' => 'Minimal 5 character!'
		]);
		$this->form_validation->set_rules('re_password', 'Repeat password', 'required|trim', [
			'required' => 'Repeat password is required!'
		]);

		if ($this->form_validation->run() == false) {
			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/profile/change_password');
			$this->load->view('admin/layout/footer');
		} else {
			$old_password = $this->input->post(htmlspecialchars('old_password'));
			$new_password = $this->input->post(htmlspecialchars('new_password'));
			$re_password  = $this->input->post(htmlspecialchars('re_password'));

			$get_admin = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();

			if ($get_admin) {
				if (password_verify($old_password, $get_admin['password'])) {
					if (!password_verify($new_password, $get_admin['password'])) {
						$password_hash = password_hash($new_password, PASSWORD_DEFAULT);

						$this->db->set('password', $password_hash);
						$this->db->set('date_update', time());
						$this->db->where('id_admin', $get_admin['id_admin']);
						$this->db->update('admin');

						$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Change password successfully!</div>');

						redirect('admin/profile');
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><i class="fas fa-times"></i> Make sure the passwords are not the same as before!</div>');

						redirect('admin/change_password');
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><i class="fas fa-times"></i> Wrong old password!</div>');

					redirect('admin/change_password');
				}
			} else {
				redirect('auth/login');
			}
		}
	}

	public function change_data()
	{
		$this->form_validation->set_rules('name', 'Nama', 'required|trim', [
			'required' => 'Name is required!'
		]);
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email', [
			'required'    => 'Email is required!',
			'valid_email' => 'Email not valid!'
		]);
		$this->form_validation->set_rules('username', 'Username', 'required|trim', [
			'required' => 'Username is required!'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|trim', [
			'required' => 'Password is required!'
		]);

		if ($this->form_validation->run() == false) {
			$data['admin'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();

			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/profile/change_data', $data);
			$this->load->view('admin/layout/footer');
		} else {
			$name     = $this->input->post(htmlspecialchars('name'));
			$email    = $this->input->post(htmlspecialchars('email'));
			$username = $this->input->post(htmlspecialchars('username'));
			$password = $this->input->post(htmlspecialchars('password'));

			$get_admin = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();

			if ($get_admin['name'] == $name AND $get_admin['email'] == $email AND $get_admin['username'] == $username) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><i class="fas fa-times"></i> Data is still the same as before!</div>');

				redirect('admin/change_data');
			}

			if ($get_admin) {
				if (password_verify($password, $get_admin['password'])) {
					$this->db->set('name', $name);
					$this->db->set('email', $email);
					$this->db->set('username', $username);
					$this->db->set('date_update', time());
					$this->db->where('id_admin', $get_admin['id_admin']);
					$this->db->update('admin');

					$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Data edited successfully!</div>');

					redirect('admin/profile');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><i class="fas fa-times"></i> Wrong password!</div>');

					redirect('admin/change_data');
				}
			} else {
				redirect('auth/login');
			}
		}
	}

	public function message()
	{
		$data['messages'] = $this->db->get('message')->result_array();

		$this->load->view('admin/layout/header');
		$this->load->view('admin/layout/sidebar');
		$this->load->view('admin/layout/topbar');
		$this->load->view('admin/message/index', $data);
		$this->load->view('admin/layout/footer');
	}

	public function read_message($id)
	{
		$this->form_validation->set_rules('message', 'Message', 'required|trim', [
			'required' => 'Message is required!'
		]);

		if ($this->form_validation->run() == false) {
			$data['message'] = $this->db->get_where('message', ['id_message' => $id])->row_array();

			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/message/view', $data);
			$this->load->view('admin/layout/footer');
		} else {
			$this->load->library('mailer');

			$send_to        = $this->input->post(htmlspecialchars('send_to'));
			$date_send      = $this->input->post(htmlspecialchars('date_send'));
			$sender_message = $this->input->post(htmlspecialchars('sender_message'));
			$message        = $this->input->post(htmlspecialchars('message'));
			$content        = $this->load->view('admin/message/content', [
				'message'        => $message,
				'date_send'      => $date_send,
				'sender_message' => $sender_message
			], true);

			$sendmail = [
				'send_to' => $send_to,
				'message' => $content
			];

			$send = $this->mailer->send($sendmail);

			if ($send['status'] == 'Success') {
				$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Message sent successfully!</div>');

				redirect('admin/read_message/' . $id);
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><i class="fas fa-times"></i> Failed to sent message!</div>');

				redirect('admin/read_message/' . $id);
			}
		}
	}

	public function delete_message($id)
	{
		$this->db->where('id_message', $id);
		$this->db->delete('message');

		$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Message deleted successfully!</div>');

		redirect('admin/message');
	}

	public function about()
	{
		$data['about'] = $this->db->get('about')->row_array();
		$data['corporate_values'] = $this->db->get('corporate_value')->result_array();
		$data['visions'] = $this->db->get('vision')->result_array();
		$data['missions'] = $this->db->get('mission')->result_array();

		$this->load->view('admin/layout/header');
		$this->load->view('admin/layout/sidebar');
		$this->load->view('admin/layout/topbar');
		$this->load->view('admin/about/index', $data);
		$this->load->view('admin/layout/footer');
	}

	public function edit_about()
	{
		$this->form_validation->set_rules('about', 'About', 'required|trim', [
			'required' => 'Please fill out the form!'
		]);

		if ($this->form_validation->run() == false) {
			$data['about'] = $this->db->get('about')->row_array();

			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/about/edit_about', $data);
			$this->load->view('admin/layout/footer');
		} else {
			$about = $this->input->post(htmlspecialchars('about'));

			if (empty($_FILES['image']['name'])) {
				$this->db->set('text_about', $about);
				$this->db->update('about');

				$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> About edited successfully!</div>');

				redirect('admin/about');
			}else{
				$config['upload_path']   = './assets/images/about';
				$config['allowed_types'] = 'jpg|png';
				$config['max_size']      = 20000;

				$upload = $this->load->library('upload', $config);

				if (!$this->upload->do_upload('image')) {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">
						<i class="fas fa-times-circle"></i> An error occurred while uploading the image!<br>
						<i class="fas fa-info-circle"></i> Please check your internet connection<br>
						<i class="fas fa-info-circle"></i> Please check your image size, Max size 20MB<br>
						<i class="fas fa-info-circle"></i> Please check your extension, must be JPG or PNG</div>');

					redirect('admin/add_news/');
				} else {
					$old_image = $this->db->get('about')->row_array();
					unlink('./assets/images/about/' . $old_image['image_about']);

					$image = $this->upload->data('file_name');

					$this->db->set('text_about', $about);
					$this->db->set('image_about', $image);
					$this->db->update('about');

					$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> About edited successfully!</div>');

					redirect('admin/about');
				}
			}
		}
	}

	public function add_cv()
	{
		$this->form_validation->set_rules('title', 'Title', 'required|trim', [
			'required' => 'Please fill out the form!'
		]);

		if ($this->form_validation->run() == false) {
			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/about/add_cv');
			$this->load->view('admin/layout/footer');
		}else{
			$title = $this->input->post(htmlspecialchars('title'));

			$data = [
				'title' => $title
			];

			$this->db->insert('corporate_value', $data);

			$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Corporate value edited successfully!</div>');

			redirect('admin/about');
		}
	}

	public function delete_cv($id)
	{
		$this->db->delete('corporate_value', ['id_cv' => $id]);
		$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Corporate value deleted successfully!</div>');

		redirect('admin/about');
	}

	public function edit_cv($id)
	{
		$this->form_validation->set_rules('title', 'Title', 'required|trim', [
			'required' => 'Please fill out the form!'
		]);

		if ($this->form_validation->run() == false) {
			$data['corporate_value'] = $this->db->get_where('corporate_value', ['id_cv' => $id])->row_array();

			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/about/edit_cv', $data);
			$this->load->view('admin/layout/footer');
		}else{
			$title = $this->input->post(htmlspecialchars('title'));

			$this->db->set('title', $title);
			$this->db->where('id_cv', $id);
			$this->db->update('corporate_value');

			$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Corporate value edited successfully!</div>');

			redirect('admin/about');
		} 
	}

	public function add_vision()
	{
		$this->form_validation->set_rules('vision', 'Vision', 'required|trim', [
			'required' => 'Please fill out the form!'
		]);

		if ($this->form_validation->run() == false) {
			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/about/add_visi');
			$this->load->view('admin/layout/footer');
		}else{
			$vision = $this->input->post(htmlspecialchars('vision'));

			$data = [
				'vision' => $vision
			];

			$this->db->insert('vision', $data);

			$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Vision added successfully!</div>');

			redirect('admin/about');
		}
	}

	public function delete_vision($id)
	{
		$this->db->delete('vision', ['id_vision' => $id]);

		$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Vision deleted successfully!</div>');

		redirect('admin/about');

	}



	public function edit_vision($id)
	{
		$this->form_validation->set_rules('vision', 'Vision', 'required|trim', [
			'required' => 'Please fill out the form!'
		]);

		if ($this->form_validation->run() == false) {

			$data['vision'] = $this->db->get_where('vision', ['id_vision' => $id])->row_array();

			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/about/edit_visi', $data);
			$this->load->view('admin/layout/footer');
		}else{
			$vision = $this->input->post(htmlspecialchars('vision'));

			$this->db->set('vision', $vision);
			$this->db->where('id_vision', $id);
			$this->db->update('vision');

			$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Vision edited successfully!</div>');

			redirect('admin/about');
		}
	}

	public function add_mission()
	{
		$this->form_validation->set_rules('mission', 'Mission', 'required|trim', [
			'required' => 'Please fill out the form!'
		]);

		if ($this->form_validation->run() == false) {
			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/about/add_misi');
			$this->load->view('admin/layout/footer');
		}else{
			$mission = $this->input->post(htmlspecialchars('mission'));

			$data = [
				'mission' => $mission
			];

			$this->db->insert('mission', $data);

			$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Mission added successfully!</div>');

			redirect('admin/about');
		}
	}

	public function delete_mission($id)
	{
		$this->db->delete('mission', ['id_mission' => $id]);

		$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Mission deleted successfully!</div>');

		redirect('admin/about');

	}



	public function edit_mission($id)
	{
		$this->form_validation->set_rules('mission', 'Mission', 'required|trim', [
			'required' => 'Please fill out the form!'
		]);

		if ($this->form_validation->run() == false) {

			$data['mission'] = $this->db->get_where('mission', ['id_mission' => $id])->row_array();

			$this->load->view('admin/layout/header');
			$this->load->view('admin/layout/sidebar');
			$this->load->view('admin/layout/topbar');
			$this->load->view('admin/about/edit_mission', $data);
			$this->load->view('admin/layout/footer');
		}else{
			$mission = $this->input->post(htmlspecialchars('mission'));

			$this->db->set('mission', $mission);
			$this->db->where('id_mission', $id);
			$this->db->update('mission');

			$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Mission edited successfully!</div>');

			redirect('admin/about');
		}
	}

}