<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function index()
	{
		$defaultProject = $this->db->order_by('id_project', 'desc')->get('project')->row_array();
		$data['defaultProjectImages'] = $this->db->get_where('project_image', ['id_project', $defaultProject['id_project']])->result_array();

		$this->load->view('layout/header');
		$this->load->view('layout/navbar');
		$this->load->view('home/index', $data);
		$this->load->view('layout/footer');
	}

	public function search()
	{
		$keyword = $this->input->post('keyword');

		$projects = $this->db->like('project_name', $keyword)->get('project')->result_array();

		foreach ($projects as $p) {
			echo "<a href='".base_url('project/view/' . $p['id_project'])."'>".$p["project_name"]."</a>";
		}
	}
	
}