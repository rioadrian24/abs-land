<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller
{

	public function index()
	{
		$this->form_validation->set_rules('company', 'Company Name', 'trim|max_length[100]', [
			'max_length' => 'Melebihi batas karakter!'
		]);
		$this->form_validation->set_rules('email', 'Email', 'required|trim|max_length[100]|valid_email', [
			'required'    => 'Harap mengisi email',
			'max_length'  => 'Melebihi batas karakter!',
			'valid_email' => 'Email tidak valid!'
		]);
		$this->form_validation->set_rules('subject', 'Subject', 'required|trim|max_length[100]', [
			'required'    => 'Harap mengisi subjek',
			'max_length'  => 'Melebihi batas karakter!',
			'valid_email' => 'Email tidak valid!'
		]);
		$this->form_validation->set_rules('message', 'Message', 'required|trim|max_length[500]', [
			'required'    => 'Harap mengisi pesan',
			'max_length'  => 'Melebihi batas karakter!',
			'valid_email' => 'Email tidak valid!'
		]);

		if ($this->form_validation->run() == false) {
			$this->load->view('layout/header');
			$this->load->view('layout/navbar');
			$this->load->view('contact/index');
			$this->load->view('layout/footer');
		} else {
			$email = $this->input->post(htmlspecialchars('email'));
			$company = $this->input->post(htmlspecialchars('company'));
			$subject = $this->input->post(htmlspecialchars('subject'));
			$message = $this->input->post(htmlspecialchars('message'));

			$data = [
				'company_name' => $company,
				'email'        => $email,
				'subject'      => $subject,
				'message'      => $message,
				'date'         => time()
			];

			$this->db->insert('message', $data);

			$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fas fa-check"></i> Berhasil mengirim pesan! Balasan akan dikirim ke email anda.</div>');

			redirect('contact');
		}
	}
	
}