<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function login()
	{
		$this->load->view('auth/login');
	}

	public function postlogin()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		if ($this->form_validation->run() != true) {
			$this->login();
		} else {
			$username = $this->input->post(htmlspecialchars('username'));
			$password = $this->input->post(htmlspecialchars('password'));

			$user = $this->db->get_where('admin', ['username' => $username])->row_array();

			if ($user) {
				if (password_verify($password, $user['password'])) {
					$data = [
						'date_update' => $user['date_update'],
						'username'    => $user['username'],
						'id_admin'    => $user['id_admin'],
						'is_login'    => true,
						'email'       => $user['email'],
						'name'        => $user['name'],
					];

					$this->session->set_userdata($data);
					redirect('admin');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
						Username atau password salah!
						</div>');
					redirect('auth/login');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
					Username atau password salah!
					</div>');
				redirect('auth/login');
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();

		redirect('auth/login');
	}
	
}