<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold">Messages</h1>

<?= $this->session->flashdata('message') ?>

<div class="card shadow-sm mb-4">
	<div class="card-body">
		<div class="table-responsive">
			<table class="table text-center dataTable">
				<thead class="bg-warning text-white">
					<tr>
						<th>No</th>
						<th>Company Name</th>
						<th>Subject</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1; foreach ($messages as $message) : ?>
					<tr>
						<td><?= $no++ ?></td>
						<td><?= $message['company_name'] ?></td>
						<td><?= $message['subject'] ?></td>
						<td>
							<a href="<?= base_url('admin/read_message/' . $message['id_message']) ?>" class="btn btn-warning btn-sm shadow-sm mb-1"><i class="fas fa-eye"></i></a>
							<button data-url="<?= base_url('admin/delete_message/' . $message['id_message']) ?>" class="btn btn-danger btn-sm mb-1 shadow-sm confirmDelete"><i class="fas fa-trash"></i></button>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
</div>

<div class="bold text-white text-center mt-5" style="position: absolute; top: 85%; left: 50%">@ABS Land All Right Reserved</div>

</div>
</div>
</div>
<!-- /.container-fluid -->