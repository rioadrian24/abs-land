<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ABS LAND | Reply Message</title>
</head>
<body>
	
	<div style="float: left; margin-right: 10px;">
		<img src="cid:logo_absland" alt="Logo" style="height: 50px;"> <br>
	</div>

	<div style="clear: both;"></div>

	<h6>From : ABS LAND</h6>
	<h6>Website : abs-land.com</h6>
	<h6>Subject : Membalas pesan anda</h6>

	<hr>

	<h6>Tanggal anda mengirim pesan : <?= $date_send ?></h6>
	<h6>Pesan anda kepada kami : </h6>

	<div style="text-align: justify;">
		<?= $sender_message ?>
	</div>

	<hr>

	<h6>Balasan : </h6>

	<div style="text-align: justify;">
		<?= $message ?>
	</div>

</body>
</html>