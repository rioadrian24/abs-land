<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold"><a href="<?= base_url('admin/message') ?>"><i class="fas fa-angle-left text-warning"></i></a> Read Message</h1>

<div class="row">
	<div class="col-lg-6">
		<div class="card shadow-sm mb-4">
			<div class="card-body">
				<h5>Message <?= date('d F Y', $message['date']) ?> | <?= date('H:i', $message['date']) ?></h5> <hr>

				<h6>From : <code><?= $message['company_name'] != null ? $message['company_name'] : 'Unknowed' ?></code></h6>
				<h6>Email : <code><?= $message['email'] ?></code></h6>
				<h6>Subject : <code><?= $message['subject'] ?></code></h6>
				<h6>Message : <code><?= $message['message'] ?></code></h6>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="card shadow-sm mb-4">
			<div class="card-body">
				<form action="" method="post">
					<h6><i class="fas fa-reply"></i> Reply Message To Gmail</h6> <hr>

					<?= $this->session->flashdata('message') ?>

					<h6>From : <code>You</code></h6>
					<h6>To : <code><?= $message['email'] ?></code></h6>
					<div class="d-none">
						<input type="text" name="send_to" value="<?= $message['email'] ?>">
						<input type="text" name="date_send" value="<?= date('d F Y', $message['date']) ?> | <?= date('H:i', $message['date']) ?>">
						<input type="text" name="sender_message" value="<?= $message['message'] ?>">
					</div>
					<div class="form-group">
						<textarea name="message" rows="5" class="form-control" placeholder="Write your reply message"></textarea>
					</div>
					<button class="btn btn-warning btn-block bold">Send Reply <i class="fas fa-paper-plane"></i></button>
				</form>

			</div>
		</div>
	</div>
</div>

<div class="bold text-white text-center mt-5" style="position: absolute; top: 85%; left: 50%">@ABS Land All Right Reserved</div>

</div>
</div>
</div>
<!-- /.container-fluid -->