<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold"><a href="<?= base_url('admin/about') ?>"><i class="fas fa-angle-left text-warning"></i></a> Edit Corporate Value</h1>

<div class="row justify-content-center">
	<div class="col-lg-6">
		<div class="card shadow-sm">
			<div class="card-body">
				<form action="" method="post">
					<div class="form-group">
						<label for="title">Corporate Value</label>
						<input type="text" class="form-control" name="title" placeholder="Enter Title" value="<?= $corporate_value['title'] ?>">
						<small class="text-danger"><?= form_error('title') ?></small>
					</div>
					<button class="btn btn-primary float-right">Save Change</button>
				</form>
			</div>
		</div>
	</div>
</div>

</div>
</div>
</div>
<!-- /.container-fluid -->