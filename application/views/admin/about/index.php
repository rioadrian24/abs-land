<!-- Page Heading -->
<h1 class="h3 text-white bold">About</h1>

<?= $this->session->flashdata('message') ?>

<?= $this->session->flashdata('message') ?>

<div class="row">
	<div class="col-lg-12">
		<div class="card shadow-sm border-0 mb-5">
			<div class="card-header bg-warning">
				<a href="<?= base_url('admin/edit_about') ?>" class="btn btn-primary shadow-sm bold">Edit About</a>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-8">
						<?= $about['text_about'] ?>
					</div>
					<div class="col-lg-4 pt-5">
						<img class="img-fluid" src="<?= base_url('assets/images/about/' . $about['image_about']) ?>" alt="About Image">
					</div>
				</div>
			</div>
		</div>

		<h1 class="h3 text-white bold">Corporate Value</h1>

		<div class="card shadow-sm border-0 mb-5">
			<div class="card-header bg-warning">
				<a href="<?= base_url('admin/add_cv') ?>" class="btn btn-primary shadow-sm bold">Add Corporate Value</a>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table dataTable text-center">
						<thead>
							<tr>
								<th>No</th>
								<th>Corporate Value</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; foreach($corporate_values as $cv) : ?>
							<tr>
								<td><?= $no++ ?></td>
								<td><?= filter($cv['title']) ?></td>
								<td>
									<a href="<?= base_url('admin/edit_cv/' . $cv['id_cv']) ?>" class="btn btn-success btn-sm mb-1 shadow-sm" title="Edit Corporate Value"><i class="fas fa-pen"></i></a>
									<button data-url="<?= base_url('admin/delete_cv/' . $cv['id_cv']) ?>" class="btn btn-danger btn-sm mb-1 shadow-sm confirmDelete"  title="Delete Corporate Value"><i class="fas fa-trash"></i></button>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<h1 class="h3 text-white bold">Vision & Mission</h1>

	<div class="card shadow-sm border-0">
		<div class="card-header bg-warning">
			<a href="<?= base_url('admin/add_vision') ?>" class="btn btn-primary shadow-sm bold">Add Vision</a>
			<a href="<?= base_url('admin/add_mission') ?>" class="btn btn-primary shadow-sm bold">Add Mission</a>
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<h3 class="text-warning bold">Vision</h3> <hr>
				<table class="table dataTable text-center">
					<thead>
						<tr>
							<th>No</th>
							<th>Vision</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $no = 1; foreach($visions as $vs) : ?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?= filter(cut($vs['vision'], 50)) ?></td>
							<td>
								<a href="<?= base_url('admin/edit_vision/' . $vs['id_vision']) ?>" class="btn btn-success btn-sm mb-1 shadow-sm" title="Edit News"><i class="fas fa-pen"></i></a>
								<button data-url="<?= base_url('admin/delete_vision/' . $vs['id_vision']) ?>" class="btn btn-danger btn-sm mb-1 shadow-sm confirmDelete"  title="Delete News"><i class="fas fa-trash"></i></button>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>

		<div class="table-responsive mt-4">
			<h3 class="text-warning bold">Mission</h3> <hr>
			<table class="table dataTable text-center">
				<thead>
					<tr>
						<th>No</th>
						<th>Mission</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1; foreach($missions as $ms) : ?>
					<tr>
						<td><?= $no++ ?></td>
						<td><?= filter(cut($ms['mission'], 50)) ?></td>
						<td>
							<a href="<?= base_url('admin/edit_mission/' . $ms['id_mission']) ?>" class="btn btn-success btn-sm mb-1 shadow-sm" title="Edit News"><i class="fas fa-pen"></i></a>
							<button data-url="<?= base_url('admin/delete_mission/' . $ms['id_mission']) ?>" class="btn btn-danger btn-sm mb-1 shadow-sm confirmDelete" title="Delete News"><i class="fas fa-trash"></i></button>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
</div>

<div class="bold text-white text-center mt-5">@ABS Land All Right Reserved</div>

</div>
</div>
</div>
<!-- /.container-fluid -->