<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold"><a href="<?= base_url('admin/about') ?>"><i class="fas fa-angle-left text-warning"></i></a> Edit About</h1>

<div class="row">
	<div class="col-lg-12">
		<div class="card shadow-sm">
			<div class="card-body">
				<form action="" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-lg-8">
							<div class="form-group">
								<textarea class="summernote" name="about"><?= $about['text_about'] ?></textarea>
								<small class="text-danger"><?= form_error('about') ?></small>
							</div>
						</div>
						<div class="col-lg-4">
							<img class="img-fluid" width="100%" src="<?= base_url('assets/images/about/' . $about['image_about']) ?>" alt="Load Image Failed">

							<div class="form-group">
								<label for="image">About Image</label>
								<div class="custom-file">
									<input type="file" class="custom-file-input" name="image" id="image">
									<label class="custom-file-label" for="image">Choose Image</label>
								</div>
								<small class="text-danger"><?= form_error('image') ?></small>
							</div>

							<button class="btn btn-primary btn-block shadow-sm bold">Save Change</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

</div>
</div>
</div>
<!-- /.container-fluid -->