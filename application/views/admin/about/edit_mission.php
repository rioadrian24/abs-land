<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold"><a href="<?= base_url('admin/about') ?>"><i class="fas fa-angle-left text-warning"></i></a> Edit Mission</h1>

<div class="row justify-content-center">
	<div class="col-lg-7">
		<div class="card shadow-sm">
			<div class="card-body">
				<form action="" method="post">
					<div class="form-group">
						<label for="mission">Mission</label>
						<textarea class="form-control" name="mission" rows="7" style="resize: none;" placeholder="Enter Mission"><?= $mission['mission'] ?></textarea>
						<small class="text-danger"><?= form_error('mission') ?></small>
					</div>
					<button class="btn btn-primary float-right">Save Change</button>
				</form>
			</div>
		</div>
	</div>
</div>

</div>
</div>
</div>
<!-- /.container-fluid -->