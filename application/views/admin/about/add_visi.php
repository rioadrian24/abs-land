<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold"><a href="<?= base_url('admin/about') ?>"><i class="fas fa-angle-left text-warning"></i></a> Add Vision</h1>

<div class="row justify-content-center">
	<div class="col-lg-7">
		<div class="card shadow-sm">
			<div class="card-body">
				<form action="" method="post">
					<div class="form-group">
						<label for="vision">Vision</label>
						<textarea class="form-control" name="vision" rows="7" style="resize: none;" placeholder="Enter Vision"></textarea>
						<small class="text-danger"><?= form_error('vision') ?></small>
					</div>
					<button class="btn btn-primary float-right">Save</button>
				</form>
			</div>
		</div>
	</div>
</div>

</div>
</div>
</div>
<!-- /.container-fluid -->