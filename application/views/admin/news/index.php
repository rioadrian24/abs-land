<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold">News</h1>

<a href="<?= base_url('admin/add_news') ?>" class="btn btn-warning shadow-sm mb-3 bold"><i class="fas fa-plus"></i> Add New Post</a>

<?= $this->session->flashdata('message') ?>

<div class="card shadow-sm">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table text-center dataTable">
        <thead class="bg-warning text-white">
          <tr>
            <th>No</th>
            <th>Thumbnail</th>
            <th>Title</th>
            <th>Posting At</th>
            <th>Updated At</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; foreach ($news as $new) : ?>
          <tr>
            <td><?= $no++ ?></td>
            <td>
              <img class="img-fluid shadow-sm" src="<?= base_url('assets/images/news/thumbnail/' . $new['thumbnail']) ?>" width="100">
            </td>
            <td><?= $new['title'] ?></td>
            <td><?= date('d F Y', $new['date']) ?></td>
            <?php if ($new['date_update'] != "-") : ?>
              <td><?= date('d F Y', $new['date_update']) ?></td>
              <?php else : ?>
                <td><?= $new['date_update'] ?></td>
              <?php endif ?>
              <td>
                <button data-id="<?= $new['id_news'] ?>" class="btn btn-warning btn-sm mb-1 shadow-sm previewNewsModal" title="News Detail"><i class="fas fa-eye"></i></button>
                <a href="<?= base_url('admin/edit_news/' . $new['id_news']) ?>" class="btn btn-success btn-sm mb-1 shadow-sm" title="Edit News"><i class="fas fa-pen"></i></a>
                <button data-url="<?= base_url('admin/delete_news/' . $new['id_news']) ?>" class="btn btn-danger btn-sm mb-1 shadow-sm confirmDelete"  title="Delete News"><i class="fas fa-trash"></i></button>
              </td>
            </tr>
          <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="bold text-white text-center mt-5">@ABS Land All Right Reserved</div>

</div>
</div>
</div>
<!-- /.container-fluid -->