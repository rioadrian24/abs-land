<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold">Tambah Postingan</h1>

<?= $this->session->flashdata('message') ?>

<div class="card shadow-sm mb-4">
	<div class="card-body">
		<form action="" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="title">Judul</label>
				<input type="text" class="form-control" name="title" id="title" placeholder="Masukan judul">
				<small class="text-danger"><?= form_error('title') ?></small>
			</div>

			<div class="form-group">
				<label for="thumbnail">Thumbnail</label>
				<div class="custom-file">
					<input type="file" class="custom-file-input" name="thumbnail" id="thumbnail">
					<label class="custom-file-label" for="thumbnail">Pilih Gambar</label>
				</div>
				<small class="text-danger"><?= form_error('thumbnail') ?></small>
			</div>

			<div class="form-group">
				<label for="summernote">Konten</label>
				<small class="text-danger"><?= form_error('content') ?></small>
				<textarea name="content" id="summernote" class="summernote"></textarea>
			</div>

			<button class="btn btn-primary mx-1 float-right">Simpan</button>
			<a href="<?= base_url('admin/news') ?>" class="btn btn-danger mx-1 float-right">Batal</a>
		</form>
	</div>
</div>

<div class="bold text-white text-center mt-5">@ABS Land All Right Reserved</div>

</div>
</div>
</div>
<!-- /.container-fluid -->