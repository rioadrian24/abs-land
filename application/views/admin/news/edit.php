<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold">Edit Postingan</h1>

<?= $this->session->flashdata('message') ?>

<div class="card shadow-sm mb-4">
	<div class="card-body">
		<form action="" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="title">Judul</label>
				<input type="text" class="form-control" name="title" id="title" placeholder="Masukan judul" value="<?= $new['title'] ?>">
				<small class="text-danger"><?= form_error('title') ?></small>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-lg-2">
						<div class="form-group">
							<label>Thumbnail</label>
							<img class="img-fluid img-thumbnail shadow-sm" src="<?= base_url('assets/images/news/thumbnail/' . $new['thumbnail']) ?>" width="100%">
						</div>
					</div>
					<div class="col-lg-10">
						<div class="form-group">
							<label for="thumbnail">Ganti Thumbnail</label>
							<div class="custom-file">
								<input type="file" class="custom-file-input" name="thumbnail" id="thumbnail">
								<label class="custom-file-label" for="thumbnail">Pilih Gambar</label>
							</div>
							<small class="text-danger"><?= form_error('thumbnail') ?></small>
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="content">Konten</label>
				<small class="text-danger"><?= form_error('content') ?></small>
				<textarea name="content" id="content" class="summernote"><?= $new['content'] ?></textarea>
			</div>

			<button class="btn btn-primary float-right mx-1">Simpan</button>
			<a href="<?= base_url('admin/news') ?>" class="btn btn-danger float-right mx-1">Batal</a>
		</form>
	</div>
</div>

<div class="bold text-white text-center mt-5">@ABS Land All Right Reserved</div>

</div>
</div>
</div>	
<!-- /.container-fluid -->