<!-- Page Wrapper -->
<div id="wrapper">

  <!-- Sidebar -->
  <ul class="navbar-nav bg-gradient-warning sidebar sidebar-dark accordion side-menu" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('admin') ?>">
      <div class="p-3">
        <img src="<?= base_url('assets/images/logo/abs2.png') ?>" class="img-fluid" alt="Logo ABS" style="margin-top: 100px;">
      </div>
    </a>
    
    <!-- Divider -->
    <hr class="sidebar-divider" style="margin-top: 90px; margin-bottom: 0;">

    <li class="nav-item bold <?= page() == 'about' ? 'menu-active active' : '' ?>">
      <a class="nav-link" href="<?= base_url('admin/about') ?>">
        <span class="nav-item-text">Abouts</span>
      </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    
    <li class="nav-item bold <?= page() == null ? 'menu-active active' : '' ?>">
      <a class="nav-link" href="<?= base_url('admin') ?>">
        <span class="nav-item-text">Dashboard</span>
      </a>
    </li>

    <hr class="sidebar-divider my-0">

    <li class="nav-item bold <?= page() == 'message' ? 'menu-active active' : '' ?>">
      <a class="nav-link" href="<?= base_url('admin/message') ?>">
        <span class="nav-item-text">Messages</span>
      </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <li class="nav-item bold <?= page() == 'news' ? 'menu-active active' : '' ?>">
      <a class="nav-link" href="<?= base_url('admin/news') ?>">
        <span class="nav-item-text">News</span>
      </a>
    </li>

    <hr class="sidebar-divider my-0">

    <li class="nav-item bold <?= page() == 'profile' ? 'menu-active active' : '' ?>">
      <a class="nav-link" href="<?= base_url('admin/profile') ?>">
        <span class="nav-item-text">Profiles</span>
      </a>
    </li>

    <hr class="sidebar-divider my-0">

    <li class="nav-item bold <?= page() == 'project' ? 'menu-active active' : '' ?>">
      <a class="nav-link" href="<?= base_url('admin/project') ?>">
        <span class="nav-item-text">Projects</span>
      </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <li class="nav-item bold">
      <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal">
        <span class="nav-item-text">Logout</span>
      </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>
    <!-- End of Sidebar -->