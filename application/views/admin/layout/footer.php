</div>
<!-- End of Main Content -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-warning text-white">
        <h5 class="modal-title" id="exampleModalLabel">Do you want to leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close" style="outline: none;">
          <span aria-hidden="true" class="text-white">×</span>
        </button>
      </div>
      <div class="modal-body">You will return to the login page!</div>
      <div class="modal-footer">
        <button class="btn btn-danger" type="button" data-dismiss="modal">Batal</button>
        <a class="btn btn-success" href="<?= base_url('auth/logout') ?>">Keluar</a>
      </div>
    </div>
  </div>
</div>

<!-- Preview News Modal -->
<div class="modal fade" id="previewNewsModal" tabindex="-1" role="dialog" aria-labelledby="previewNewsModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-warning bold font-weight-bold" id="previewNewsModalTitle">Loading title...</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="previewNewsModalContent">
        Loading content...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger shadow-sm" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/SBAdmin/vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/SBAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/SBAdmin/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/SBAdmin/js/sb-admin-2.min.js') ?>"></script>


<script src="<?= base_url('assets/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?= base_url('assets/datatables/datatables-demo.js') ?>"></script>

<script src="<?= base_url('assets/summernote/summernote-bs4.js') ?>"></script>

<script>
  $('.summernote').summernote({
    height: "300px",
    callbacks: {
      onImageUpload: function(image) {
        uploadImage(image[0]);
      },
      onMediaDelete : function(target) {
        deleteImage(target[0].src);
      }
    }
  });

  function uploadImage(image) {
    let data = new FormData();
    data.append("image", image);

    $.ajax({
      url: "<?= base_url('admin/sn_upload_image')?>",
      cache: false,
      contentType: false,
      processData: false,
      data: data,
      type: "POST",
      success: function(url) {
        $('.summernote').summernote("insertImage", url);
      },
      error: function(data) {
        console.log(data);
      }
    });
  }

  function deleteImage(src) {
    $.ajax({
      data: {src : src},
      type: "POST",
      url: "<?= base_url('admin/sn_delete_image')?>",
      cache: false,
      success: function(response) {
        console.log(response);
      }
    });
  }

  $('.confirmDelete').click(function() {
    let url = $(this).data('url');

    Swal.fire({
      title: 'Apakah anda yakin',
      text: "Data yang terhapus tidak dapat kembali!",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        window.location.href = url
      }
    })
  })

  $('.previewNewsModal').click(function() {
    let id = $(this).data('id')

    $.ajax({
      url: "<?= base_url('admin/preview_news') ?>",
      data: {id: id},
      dataType: "json",
      type: "POST",
      success: function(data) {
        $('#previewNewsModal').modal('show')

        $('#previewNewsModalTitle').html(data.title)

        $('#previewNewsModalContent').html(data.content)
      }
    })
  })

  $('input[type="file"]').change(function(e){
    var fileName = e.target.files[0].name;
    $('.custom-file-label').html(fileName);
  });
</script>

</body>

</html>