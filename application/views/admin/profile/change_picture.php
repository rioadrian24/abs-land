<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold">Change Picture</h1>

<div class="row">
	<div class="col-lg-3">
		<div class="card shadow-sm mb-3" style="height: 240px !important">
			<div class="card-body text-center">
				<img src="<?= getImage() ?>" class="img-fluid img-thumbnail rounded-circle shadow-sm mt-3" alt="Profile image" width="150">
			</div>
		</div>
	</div>
	<div class="col-lg-9">
		<div class="card shadow-sm mb-4">
			<div class="card-header text-success">
				<h3 class="text-warning bold">Change Picture</h3>
			</div>
			<div class="card-body">
				<form action="" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="image" id="image">
							<label class="custom-file-label" for="image">Pilih Gambar</label>
						</div>
						<small class="text-danger"><?= form_error('image') ?></small>
						<label>
							<input type="checkbox" name="default_image"> Gambar default
						</label>
					</div>
					<button class="btn btn-primary float-right mx-1 mb-2">Simpan</button>
					<a href="<?= base_url('admin/profile') ?>" class="btn btn-danger float-right mx-1 mb-2">Batal</a>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="bold text-white text-center mt-5" style="position: absolute; top: 85%; left: 50%">@ABS Land All Right Reserved</div>

</div>
</div>
</div>
<!-- /.container-fluid -->