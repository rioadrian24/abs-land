<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold">Change Data</h1>

<div class="row">
	<div class="col-lg-3">
		<div class="card shadow-sm mb-3" style="height: 315px !important">
			<div class="card-body text-center">
				<img src="<?= getImage() ?>" class="img-fluid img-thumbnail rounded-circle shadow-sm mt-5" alt="Profile image" width="150px;">
			</div>
		</div>
	</div>
	<div class="col-lg-9">
		<div class="card shadow-sm mb-4 border-0">
			<div class="card-header bg-warning">
				<h3 class="text-white bold">Change Data</h3>
			</div>
			<div class="card-body">
				<form action="" method="post">

					<?= $this->session->flashdata('message'); ?>

					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Nama</label>
								<input type="text" class="form-control" name="name" value="<?= filter($admin['name']) ?>">
								<small class="text-danger"><?= form_error('name') ?></small>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Email</label>
								<input type="text" class="form-control" name="email" value="<?= filter($admin['email']) ?>">
								<small class="text-danger"><?= form_error('email') ?></small>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label>Username</label>
								<input type="text" class="form-control" name="username" value="<?= filter($admin['username']) ?>">
								<small class="text-danger"><?= form_error('username') ?></small>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Konfirmasi Password</label>
								<input type="password" class="form-control" name="password">
								<small class="text-danger"><?= form_error('password') ?></small>
							</div>
						</div>
					</div>
					<button class="btn btn-primary float-right mx-1">Simpan</button>
					<a href="<?= base_url('admin/profile') ?>" class="btn btn-danger float-right mx-1">Batal</a>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="bold text-white text-center mt-5" style="position: absolute; top: 85%; left: 50%">@ABS Land All Right Reserved</div>

</div>
</div>
</div>
<!-- /.container-fluid -->