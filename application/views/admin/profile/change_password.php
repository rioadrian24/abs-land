<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold">Change Password</h1>

<div class="row">
	<div class="col-lg-3">
		<div class="card shadow-sm mb-3" style="height: 230px !important">
			<div class="card-body text-center">
				<img src="<?= getImage() ?>" class="img-fluid img-thumbnail rounded-circle shadow-sm mt-3" alt="Profile image" width="150">
			</div>
		</div>
	</div>
	<div class="col-lg-9">
		<div class="card shadow-sm mb-4">
			<div class="card-header text-success">
				<h3 class="text-warning bold">Change Password</h3>
			</div>
			<div class="card-body">
				<form action="" method="post">

					<?= $this->session->flashdata('message'); ?>

					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">
								<label>Password Lama</label>
								<input type="password" class="form-control" name="old_password">
								<small class="text-danger"><?= form_error('old_password') ?></small>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Password Baru</label>
								<input type="password" class="form-control" name="new_password">
								<small class="text-danger"><?= form_error('new_password') ?></small>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">
								<label>Ulangi Password Baru</label>
								<input type="password" class="form-control" name="re_password">
								<small class="text-danger"><?= form_error('re_password') ?></small>
							</div>
						</div>
					</div>
					<button class="btn btn-primary float-right mx-1">Simpan</button>
					<a href="<?= base_url('admin/profile') ?>" class="btn btn-danger float-right mx-1">Batal</a>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="bold text-white text-center mt-5" style="position: absolute; top: 85%; left: 50%">@ABS Land All Right Reserved</div>

</div>
</div>
</div>
<!-- /.container-fluid -->