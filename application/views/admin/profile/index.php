<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold">Profile Setting</h1>

<div class="row">
	<div class="col-lg-3">
		<div class="card shadow-sm mb-3" style="height: 320px !important">
			<div class="card-body text-center">
				<img src="<?= getImage() ?>" class="img-fluid img-thumbnail rounded-circle shadow-sm mt-3" alt="Profile image" width="150">
				<a href="<?= base_url('admin/change_picture') ?>" class="btn btn-info btn-block bold" style="margin-top: 50px">Change Picture</a>
			</div>
		</div>
	</div>
	<div class="col-lg-9">
		<div class="card shadow-sm mb-4 border-0">
			<div class="card-header text-success bg-warning">
				<h4 class="d-inline text-white bold"><?= filter($admin['name']) ?></h4>
				<a href="<?= base_url('admin/change_data') ?>" class="btn btn-primary btn-sm float-right bold">Ubah Data</a>
			</div>
			<div class="card-body">
				<?php if ($this->session->flashdata('message')) : ?>
					<div class="mb-2">
						<?= $this->session->flashdata('message') ?>
					</div>
				<?php endif ?>
				<div class="table-responsive">
					<table class="table table-striped text-left">
						<thead>
							<tr>
								<th>Email <span class="float-right">:</span> </th>
								<th><?= filter($admin['email']) ?></th>
							</tr>
							<tr>
								<th>Username <span class="float-right">:</span> </th>
								<th><?= filter($admin['username']) ?></th>
							</tr>
							<tr>
								<th>Updated <span class="float-right">:</span> </th>
								<?php if ($this->session->userdata('date_update') != "-") : ?>
									<th><?= date('d F Y', filter($admin['date_update'])) ?></th>
									<?php else : ?>
										<th><?= filter($admin['date_update']) ?></th>
									<?php endif ?>
								</tr>
								<tr>
									<th>Password <span class="float-right">:</span> </th>
									<th><a href="<?= base_url('admin/change_password') ?>" class="btn btn-success btn-sm bold">Change Password</a></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="bold text-white text-center mt-5" style="position: absolute; top: 85%; left: 50%">@ABS Land All Right Reserved</div>

</div>
</div>
</div>
<!-- /.container-fluid -->