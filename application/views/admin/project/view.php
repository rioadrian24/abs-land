	<!-- Page Heading -->
	<h1 class="h3 mb-4 text-white bold"><a href="<?= base_url('admin/project') ?>"><i class="fas fa-angle-left text-warning"></i></a> <?= strtoupper(filter($project['project_name'])) ?></h1>

	<div class="row">
		<div class="col-lg-12">

			<?= $this->session->flashdata('message'); ?>

			<div class="card shadow-sm mb-4">
				<div class="card-body">
					<form action="" method="post" enctype="multipart/form-data">
						<div class="row">
							<div class="col-9 col-lg-11">
								<div class="custom-file">
									<input type="file" class="custom-file-input" name="image" id="image">
									<label class="custom-file-label" for="image">Pilih Gambar</label>
								</div>
								<small class="text-danger"><?= form_error('image') ?></small>
							</div>
							<div class="col-3 col-lg-1">
								<button class="btn btn-primary btn-block shadow-sm"><i class="fas fa-plus"></i></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-body">
					<?php if (!$project_image) : ?>
						<h4 class="text-center text-warning bold"><i class="fas fa-times-circle"></i> NO PROJECT IMAGE</h4>
						<?php else : ?>
							<div class="row">
								<?php foreach ($project_image as $pi) : ?>
									<div class="col-lg-3 mb-3">
										<div class="hover-image">
											<img src="<?= base_url('assets/images/projects/' . $pi['image_name']) ?>" class="img-fluid" alt="Image">
											<button data-url="<?= base_url('admin/delete_project_image/' . $pi['id_project_image']) . "and" . $pi['id_project'] ?>" class="btn btn-danger btn-sm mb-1 shadow-sm confirmDelete"><i class="fas fa-trash"></i> <span class="font-weight-bold">HAPUS</span></button>
										</div>
									</div>
								<?php endforeach ?>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>

	</div>
	</div>
	</div>
	<!-- /.container-fluid -->