<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold">Projects</h1>

<a href="<?= base_url('admin/add_project') ?>" class="btn btn-warning shadow-sm mb-3 bold"><i class="fas fa-plus"></i> Add New Project</a>

<?= $this->session->flashdata('message') ?>

<div class="card mb-4 shadow-sm">
 <div class="card-body">
  <div class="table-responsive">
   <table class="table text-center dataTable">
    <thead class="bg-warning text-white">
     <tr>
      <th>No</th>
      <th>Banner</th>
      <th>Project Name</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $no = 1; foreach ($project as $p) : ?>
    <tr>
      <td><?= $no++; ?></td>
      <td>
        <?php if ($p['banner'] == null) : ?>
          <img class="img-fluid img-thumbnail shadow-sm" src="<?= base_url('assets/images/projects/default/banner.jpg') ?>" width="150">
          <?php else : ?>
            <img class="img-fluid img-thumbnail shadow-sm" src="<?= base_url('assets/images/projects/' . $p['banner']) ?>" width="150">
          <?php endif ?>
        </td>
        <td><?= $p['project_name']; ?></td>
        <td>
          <a href="<?= base_url('admin/view_project/' . $p['id_project']) ?>" class="btn btn-primary btn-sm mb-1 shadow-sm" title="Add Project Image"><i class="fas fa-image"></i></a>
          <a href="<?= base_url('admin/edit_project/' . $p['id_project']) ?>" class="btn btn-success btn-sm shadow-sm mb-1" title="Edit Project"><i class="fas fa-pen"></i></a>
          <button data-url="<?= base_url('admin/delete_project/' . $p['id_project']) ?>" class="btn btn-danger btn-sm mb-1 shadow-sm confirmDelete" title="Delete Project"><i class="fas fa-trash"></i></button>
        </td>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
</div>
</div>
</div>

<div class="bold text-white text-center mt-5">@ABS Land All Right Reserved</div>

</div>
</div>
</div>
<!-- /.container-fluid -->