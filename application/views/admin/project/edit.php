<!-- Page Heading -->
<h1 class="h3 mb-4 text-white bold">Edit Project</h1>

<div class="row justify-content-center">
	<div class="col-lg-12">
		<div class="card shadow-sm">
			<div class="card-body">
				<form action="" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<h4 class="text-center"><?= $project['project_name'] ?> Banner</h4>
						<?php if ($project['banner'] != null) { ?>
							<img class="img-fluid img-thumbnail shadow-sm" src="<?= base_url('assets/images/projects/' . $project['banner']) ?>" alt="Banner">
						<?php } else { ?>
							<img class="img-fluid img-thumbnail shadow-sm" src="<?= base_url('assets/images/projects/default/banner.jpg') ?>" alt="Banner">
						<?php } ?>
					</div>

					<div class="form-group">
						<label>Change Banner <span class="text-success">(optional)</span></label>
						<div class="custom-file">
							<input type="file" class="custom-file-input" name="banner" id="banner">
							<label class="custom-file-label" for="banner">Choose Image</label>
						</div>
						<label>
							<input type="checkbox" name="default_image"> Choose default banner
						</label>
						<small class="text-danger"><?= form_error('banner') ?></small>
					</div>

					<div class="form-group">
						<label for="maps">Google Maps Link</label>
						<input type="text" class="form-control" name="maps" id="maps" placeholder="Enter Link" value="<?= $project['maps_link'] ?>">
						<small class="text-danger"><?= form_error('maps') ?></small>
					</div>

					<div class="form-group">
						<label for="project_name">Project Name</label>
						<input type="text" class="form-control" name="project_name" id="project_name" placeholder="Nama project" value="<?= filter($project['project_name']) ?>">
						<small class="text-danger"><?= form_error('project_name') ?></small>
					</div>

					<div class="form-group">
						<label for="description">Project Description</label>
						<textarea name="description" id="description" class="summernote"><?= $project['project_description'] ?></textarea>
						<small class="text-danger"><?= form_error('description') ?></small>
					</div>

					<button class="btn btn-primary float-right ml-1">Save Change</button>
					<a href="<?= base_url('admin/project') ?>" class="btn btn-danger float-right mr-1">Cancel</a>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="bold text-white text-center mt-5">@ABS Land All Right Reserved</div>

</div>
</div>
</div>
<!-- /.container-fluid -->