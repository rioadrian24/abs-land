<div class="jumbotron jumbotron-fluid project-banner"></div>

<div class="container mb-5">
  <div class="row justify-content-center">
    <div class="col-lg-12">
      <h1 class="text-center m-0 news">CONTACT US</h1>
    </div>
  </div>

  <div class="row mb-5">
    <div class="col-lg-6">
      <h1 class="text-center office mb-5 titlev">SEND A MESSAGE</h1>

      <?= $this->session->flashdata('message') ?>

      <form action="" method="post">
        <div class="contact-form">
          <div class="form-group">
            <input type="text" class="custom-input" name="company" placeholder="COMPANY NAME">
            <small class="text-danger d-block"><?= form_error('company') ?></small>
          </div>
          
          <div class="form-group">
            <input type="text" class="custom-input" name="email" placeholder="EMAIL ADDRESS">
            <small class="text-danger d-block"><?= form_error('email') ?></small>
          </div>
          
          <div class="form-group">
            <input type="text" class="custom-input" name="subject" placeholder="SUBJECT">
            <small class="text-danger d-block"><?= form_error('subject') ?></small>
          </div>
          
          <div class="form-group">
            <textarea class="custom-input" name="message" rows="7" placeholder="MESSAGE ..."></textarea>
            <small class="text-danger d-block"><?= form_error('message') ?></small>
          </div>

          <button class="btn btn-warning btn-block btn-lg text-white">Kirim <i class="fas fa-paper-plane"></i></button>
        </div>
      </form>
    </div>
    <div class="col-lg-6 text-small text-center">
      <h1 class="text-center office mb-5 titlev">OUR OFFICE</h1>

      <h4 class="text-center">PT. ARTHA BUANA SAMUDERA</h4>
      <h4 class="text-center">Jl. Pluit Barat | No. 51. Jakarta Utara</h4>
      <h4 class="text-center">DKI Jakarta 14450.</h4>
      <h4 class="text-center mb-4">INDONESIA</h4>
      <h4 class="text-center"><i class="fas fa-phone"></i> 021 - 66603546</h4>
      <h4 class="text-center"><i class="fas fa-envelope"></i> info@abs-land.com</h4>

      <div class="d-none d-lg-block">
        <img class="img-fluid mt-5" width="300" src="<?= base_url('assets/images/logo/abs2.png') ?>" alt="ABS LOGO">
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12 text-center">
      <h1 class="text-center office mb-5 titlev">ON MAPS</h1>

      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15867.622384397751!2d106.7765362520909!3d-6.143381595717622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d904b1f2d13%3A0x6788424cc38b1033!2sJl.%20Pluit%20Barat%20I%20No.51%2C%20RT.23%2FRW.8%2C%20Pluit%2C%20Kec.%20Penjaringan%2C%20Kota%20Jkt%20Utara%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2014450!5e0!3m2!1sen!2sid!4v1570434517241!5m2!1sen!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

      <div class="d-block d-lg-none">
        <img class="img-fluid mt-5" width="300" src="<?= base_url('assets/images/logo/abs2.png') ?>" alt="ABS LOGO">
      </div>
    </div>
  </div>
</div>