<footer>
  <h6 class="text-center">@ABS Land All Right Reserved</h6>
</footer>

<script src="<?= base_url('assets/js/bootstrap/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/js/popper/popper.min.js') ?>"></script>
<script src="<?= base_url('assets/js/custom/script.js') ?>"></script>

<!-- vendors -->
<script src="<?= base_url('assets/owl-carousel/highlight.js') ?>"></script>
<script src="<?= base_url('assets/owl-carousel/app.js') ?>"></script>
<script src="<?= base_url('assets/chocolate/jquery.chocolate.js') ?>"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<!-- Drop effect -->
<script src="<?= base_url('assets/drop-effect/js/jquery.dropdown.js') ?>"></script>

<script>
  $(document).ready(function() {
    $('.input-search').keyup(function() {
      let keyword = $(this).val()

      if (keyword == "") {
        $('.search-result').css('display', 'none')
      } else {
        $.ajax({
          url: '<?= base_url('home/search') ?>',
          type: 'post',
          data: 'keyword=' + keyword,
          dataType: 'html',
          success: function(result) {
            console.log(result)
            if (result == "") {
              $('.search-result').css('display', 'block')
              $('.search-result').html('<p><i class="fas fa-times"></i> Tidak ada hasil</p>')
            } else {
              $('.search-result').css('display', 'block')
              $('.search-result').html(result)
            }
          }
        })
      }

    })
  })
</script>

</body>
</html>
