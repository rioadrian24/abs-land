<!DOCTYPE html>
<html lang="en">
<head>
  <title>ABS Land | Website Company</title>
  <link rel="shortcut icon" href="<?= base_url('assets/images/favicon/favicon.png') ?>">

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="<?= base_url('assets/owl-carousel/owl.carousel.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/owl-carousel/owl.theme.default.min.css') ?>">

  <!-- CSS -->
  <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/font-awesome/css/all.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/chocolate/chocolate.css') ?>">
  <link rel="stylesheet" href="<?= base_url('assets/drop-effect/css/dropeffetct4.css') ?>">
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
  <!-- <link rel="stylesheet" href="http://localhost/studio/assets/js/bootstrap.min.js"> -->
  <link href="https://fonts.googleapis.com/css?family=Red+Hat+Display:400,700&display=swap" rel="stylesheet">
  <!-- Script -->
  <script src="<?= base_url('assets/js/jquery/jquery-3.3.1.min.js') ?>"></script>
  <script src="<?= base_url('assets/js/jquery/jquery.easing.1.3.js') ?>"></script>
  <script src="<?= base_url('assets/owl-carousel/owl.carousel.min.js') ?>"></script>
  <script src="<?= base_url('assets/drop-effect/js/modernizr.custom.63321.js') ?>"></script>
</head>
<body id="home">
