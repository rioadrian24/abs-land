<nav class="navbar navbar-expand-lg navbar-light shadow-sm fixed-top py-0">

  <div class="mr-auto">
    <img src="assets/images/logo/abs2.png" class="img-fluid p-3" width="200">
  </div>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="d-none d-md-inline-block d-lg-inline-block">
    <ul class="navbar-nav navbar-lg-menu mx-auto bold">
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#home">HOME</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#about">ABOUT US</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#project">PROJECTS</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#news">NEWS</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#contact">CONTACT US</a>
      </li>
    </ul>
  </div>

  <div class="ml-auto d-none d-md-block d-lg-block">
    <div class="social-media">
      <a href="" class="facebook-icon">
        <i class="fab fa-facebook-f fa-lg"></i>
      </a>

      <a href="" class="twitter-icon">
        <i class="fab fa-twitter fa-lg"></i>
      </a>

      <a href="" class="instagram-icon">
        <i class="fab fa-instagram fa-lg"></i>
      </a>

      <a href="" class="whatsapp-icon">
        <i class="fab fa-whatsapp fa-lg"></i>
      </a>
    </div>
  </div>

</nav>
