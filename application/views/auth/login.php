<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>ABS LAND | Authentication</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url('assets/SBAdmin/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url('assets/SBAdmin/css/sb-admin-2.min.css') ?>" rel="stylesheet">

</head>

<body style="background: url('<?= base_url('assets/images/bg-login/bg.jpg') ?>'); background-size: cover;">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-5 col-lg-5 col-md-5">

        <div class="card o-hidden border-0 shadow-sm" style="margin: 100px auto">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <img src="<?= base_url('assets/images/logo/abs2.png') ?>" class="img-fluid mb-4" style="width: 200px;">
                  </div>
                  <?= $this->session->flashdata('message') ?>
                  <form action="<?= base_url('auth/postlogin') ?>" method="post" class="user">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user shadow-sm" name="username" id="username" placeholder="Username">
                      <?= form_error('username', '<small class="text-danger pl-3">', '</small>') ?>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user shadow-sm" name="password" id="password" placeholder="Password">
                      <?= form_error('password', '<small class="text-danger pl-3">', '</small>') ?>
                    </div>
                    <button type="submit" class="btn btn-user btn-block text-white" style="background: rgb(255, 203, 69, .9);">
                      Login <i class="fas fa-sign-in-alt"></i>
                    </button>
                    <a href="<?= base_url('') ?>" class="d-block text-center mt-2"><i class="fas fa-angle-left"></i> Kembali ke homepage</a>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <div class="bold text-white text-center mt-5 font-weight-bold" style="position: absolute; top: 85%; left: 50%; transform: translate(-50%,  -10%); ">@ABS Land All Right Reserved</div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url('assets/SBAdmin/vendor/jquery/jquery.min.js') ?>"></script>
  <script src="<?= base_url('assets/SBAdmin/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url('assets/SBAdmin/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url('assets/SBAdmin/js/sb-admin-2.min.js') ?>"></script>

</body>

</html>
