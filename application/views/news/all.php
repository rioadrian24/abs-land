<div class="jumbotron jumbotron-fluid project-banner mb-3"></div>

<div class="col-md-12">
  <h1 class="text-center titlev all mb-5">ALL NEWS</h1>
  <div class="container">
    <div class="row">
      <?php foreach ($news as $new) : ?>
        <div class="col-md-4 p-1" data-aos="fade-up" >
          <div class="card mb-3 border-0">
            <div class="card-body p-2">
              <a href="<?= base_url('news/read/' . $new['id_news']) ?>">
                <div class="hover-zoom-image">
                  <img class="img-fluid" src="<?= base_url('assets/images/news/thumbnail/' . $new['thumbnail']) ?>" width="100%" height="250px" alt="Refresh jika tidak ada gambar">
                </div>
              </a>
            </div>
            <div class="card-title p-2">
              <h6 class="mt-2"><?= date('d F Y', $new['date']) ?></h6>
              <h1 class="mb-4"><a href="<?= base_url('news/read/' . $new['id_news']) ?>" class="judul"><?= cut($new['title'], 32) ?></a></h1>
              <a href="<?= base_url('news/read/' . $new['id_news']) ?>" class="read">READ MORE</a>
            </div>
          </div>
        </div>
      <?php endforeach ?>

    </div>
  </div>
  
</div>