<div class="jumbotron jumbotron-fluid project-banner"></div>

<div class="container">
	<h1 class="text-center news my-5"><?= filter($news['title']) ?></h1>
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header bg-white">
					<h6><i class="fas fa-clock"></i> Posting at <?= filter(date('d F Y', $news['date'])) ?></h6>
				</div>
				<div class="card-body">
					<h6 class="our mb-3"><?= filter($news['title']) ?></h6>
					<hr>

					<p> <?= $news['content'] ?> </p>

					<div class="float-right">
						<h6 class="share">SHARE NEWS</h6>
						<a href="" class="icon mb-5"><i class="fab fa-facebook-f"></i></a>
						<a href="" class="icon mb-5"><i class="fab fa-twitter"></i></a>
						<a href="" class="icon mb-5"><i class="fab fa-pinterest-p"></i></a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card mb-4">
				<div class="card-header bg-white">
					<i class="fas fa-tags"></i> Tags
				</div>
				<div class="card-body">
					<ul class="tags">
						<li><a href="#" class="tag">HTML</a></li>
						<li><a href="#" class="tag">CSS</a></li>
						<li><a href="#" class="tag">JavaScript</a></li>
						<li><a href="#" class="tag">PHP</a></li>
					</ul>
				</div>
			</div>

			<div class="card">
				<div class="card-header bg-white">
					<i class="fas fa-folder"></i> Latest News
				</div>
				<div class="card-body">
					<?php foreach ($five_news as $get) : ?>
						<div class="row mb-3">
							<div class="col-lg-4">
								<a href="<?= base_url('news/read/' . $get['id_news']) ?>">
									<img src="<?= base_url('assets/images/news/thumbnail/' . $get['thumbnail']) ?>" class="img-fluid latest-news" alt="Thumbnail">
								</a>
							</div>
							<div class="col-lg-8 pl-0">
								<h6><a href="<?= base_url('news/read/' . $get['id_news']) ?>"><?= $get['title'] ?></a></h6>
								<small><?= cut($get['content'], 70) ?></small>
							</div>
						</div>
					<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>
</div>
