<div class="jumbotron jumbotron-fluid project-banner mb-3"></div>
<h1 class="text-center m-0 news">NEWS</h1>
<h6 class="text-center titlev">FEATURED</h6>
<div class="col-md-12 mt-5">
  <div class="row" style="padding-right: 6%">

    <div class="col-md-8 p-0" data-aos="zoom-in-up">
      <div class="image-newest">
        <img src="<?= base_url('assets/images/news/thumbnail/' . $feature['thumbnail']) ?>" class="img-fluid" alt="Thumbnail">
      </div>
    </div>

    <div class="col-md-4">
      <div class="card-body p-0" data-aos="fade-up">
        <h6><?= date('d F Y', filter($feature['date'])) ?></h6>
        <h1 class="our mb-4"><a href="<?= base_url('news/read/' . $feature['id_news']) ?>" class="judul"><?= cut($feature['title'], 32) ?></a></h1>
        <p class="paragraf"><?= cut($feature['content'], 500) ?></p>

        <a href="<?= base_url('news/read/' . $feature['id_news']) ?>" class="read">READ MORE</a>
      </div>
    </div>
  </div>
</div>

<div class="col-md-12">
  <h1 class="text-center titlev all mb-5">ALL NEWS</h1>
  <div class="container">
    <div class="row">
      <?php foreach ($news as $new) : ?>
          <div class="col-md-4 p-1" data-aos="fade-up" >
            <div class="card mb-3 border-0">
              <div class="card-body p-2">
                <a href="<?= base_url('news/read/' . $new['id_news']) ?>">
                  <div class="hover-zoom-image">
                    <img class="img-fluid" src="<?= base_url('assets/images/news/thumbnail/' . $new['thumbnail']) ?>" width="100%" height="250px" alt="Refresh jika tidak ada gambar">
                  </div>
                </a>
              </div>
              <div class="card-title news-card-title p-2">
                <h6 class="mt-2"><?= date('d F Y', $new['date']) ?></h6>
                <h1 class="mb-4"><a href="<?= base_url('news/read/' . $new['id_news']) ?>" class="judul"><?= cut($new['title'], 32) ?></a></h1>
                <a href="<?= base_url('news/read/' . $new['id_news']) ?>" class="read">READ MORE</a>
              </div>
            </div>
          </div>
      <?php endforeach ?>

    </div>
    <div class="row justify-content-center">
      <a href="<?= base_url('news/allnews') ?>" class="mnews my-5 mt-5 text-center">MORE NEWS</a>
    </div>
  </div>
  
</div>