<div class="jumbotron jumbotron-fluid project-banner mb-3"></div>

<h1 class="text-center m-0 news">PROJECTS</h1>
<h6 class="text-center titlev">ALL PROJECTS</h6>

<div class="container my-5">
	<div class="row">
		<?php foreach ($projects as $project) : ?>
			<div class="col-lg-4" data-aos="fadeUp">
				<div class="card shadow text-center mb-4">
					<div class="card-body">
						<h4 class="card-title"><?= $project['project_name'] ?></h4>
						<a href="<?= base_url('project/view/' . $project['id_project']) ?>" class="btn btn-primary">View Project <i class="fas fa-arrow-right"></i></a>
					</div>
				</div>
			</div>
		<?php endforeach ?>
	</div>
</div>