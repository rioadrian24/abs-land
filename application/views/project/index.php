<?php if ($project['banner'] == null) : ?>
	<div class="jumbotron jumbotron-fluid project-banner"></div>
	<?php else : ?>
		<div class="jumbotron jumbotron-fluid" style="background: url(<?= base_url('assets/images/projects/' . $project['banner']) ?>); background-size: cover; height: 400px; background-position: 0px -50px; background-attachment: fixed;">
		</div>
	<?php endif ?>

	<h4 class="text-center titlev2" style="margin-bottom: 60px;"><?= strtoupper($project['project_name']) ?></h4>


	<div class="container mb-5">
		<div class="row justify-content-center">
			<div class="col-lg-12 mb-5 text-center">
				<span><?= $project['project_description'] ?></span>
				<br>
				<button type="button" class="btn btn-warning bold text-white show-maps"><i class="fas fa-map-marker-alt"></i> View Location in Google Maps</button>

				<div class="hidden project-maps mt-5">
					<iframe src="<?= $project['gmaps_link'] ?>" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				</div>

			</div>
			<?php if ($project_image) { ?>
				<?php foreach ($project_image as $pi) : ?>
					<div class="col-lg-4 mb-4">
						<div class="gallery-grid hover-image">
							<a href="<?= base_url('assets/images/projects/' . $pi['image_name']) ?>">
								<img src="<?= base_url('assets/images/projects/' . $pi['image_name']) ?>" class="img-fluid" alt="image-error">
								<button class="btn btn-warning text-white">Lihat Detail</button>
							</a>
						</div>
					</div>
				<?php endforeach ?>
			<?php } else { ?>
				<?php for ($i = 0; $i <= 5; $i++) : ?>
					<div class="col-lg-4 mb-4 text-center">
						<div class="virtual-image">
							<h3>No Image</h3>
						</div>
					</div>
				<?php endfor ?>
			<?php } ?>
		</div>
	</div>