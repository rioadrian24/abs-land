<div class="jumbotron jumbotron-fluid project-banner mb-3"></div>

<h1 class="text-center m-0 news">PROJECTS</h1>
<h6 class="text-center titlev">ALL PROJECTS</h6>

<div class="container my-5">
	<div class="row">
		<?php if ($projects) : ?>
			<?php foreach ($projects as $project) : ?>
				<div class="col-lg-4" data-aos="fadeUp">
					<div class="card shadow text-center mb-4">
						<div class="card-body">
							<h4 class="card-title"><?= $project['project_name'] ?></h4>
							<a href="<?= base_url('project/view/' . $project['id_project']) ?>" class="btn btn-warning font-weight-bold text-white">View Project <i class="fas fa-arrow-right"></i></a>
						</div>
					</div>
				</div>
			<?php endforeach ?>
			<?php else : ?>
				<div class="col-lg-12 text-center">
					<h3><i class="fas fa-search"></i> Hasil pencarian "<u><?= $keyword ?></u>" Tidak Ditemukan!</h3>
					<a href="<?= base_url('project') ?>" class="btn btn-warning my-5">Lihat Semua Project <i class="fas fa-arrow-right"></i></a>
				</div>
			<?php endif ?>
		</div>
	</div>