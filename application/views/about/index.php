<div class="jumbotron jumbotron-fluid project-banner"></div>
<h1 class="text-center mb-5 myabout">ABOUT US</h1>
<div class="col-md-12 us mb-5">
  <div class="row mb-5">
    <div class="col-md-8 about">
      <?= $about['text_about'] ?>
    </div>
    <div class="col-md-4" style="height: 300px;">
      <img src="<?= base_url('assets/images/about/' . $about['image_about']) ?>" class="img-fluid">
    </div>
  </div>
</div>
<div class="jumbotron jumbotron-fluid j1">
  <h1 class="text-center title">CORPORATE VALUE</h1>
  <div class="col-md-12 cv">
    <div class="row">

      <?php foreach ($corporate_values as $cv) : ?>
        <div class="col-md-6 mt-4">
          <div class="card-body m-3 cv-card cv-shadow" data-aos="fade-up" data-aos-delay="200">
            <h2><?= filter($cv['title']) ?></h2>
            <p><?= filter($cv['description']) ?></p>
          </div>
        </div>
      <?php endforeach ?>

    </div>
  </div>
</div>

<div class="col-md-12 vimi">
  <div class="row justify-content-center">
    <div class="col-md-9 vision">

      <h1 class="text-center titlev">OUR VISION</h1>

      <?php foreach ($visions as $vision) : ?>
        <h6 class="visi">
          <img src="<?= base_url('assets/images/logo/abs2k.png') ?>" class="abs" alt="Icon">
          <?= filter($vision['vision']) ?>
        </h6>
      <?php endforeach ?>

      <h1 class="text-center titlev">OUR MISSION</h1>

      <?php foreach ($missions as $mission) : ?>
        <h6 class="misi">
          <img src="<?= base_url('assets/images/logo/abs2k.png') ?>" class="abs" alt="Icon">
          <?= filter($mission['mission']) ?>
        </h6>
      <?php endforeach ?>

    </div>
  </div>
</div>