<div class="jumbotron banner-jumbotron mb-0">
	<!-- <p>@ABS Land All Right Reserved</p> -->
</div>

<section class="py-5" id="about">
  <div class="container">
    <h1 class="text-center bold mb-5">ABOUT US</h1>
    <div class="row">
      <div class="col-lg-8">
        <p class="text-about">PT. Artha Buana Samudera ( ABS Land ) adalah perusahaan yang bergerak di bidang pengembangan lahan untuk perumahan dan komersil. ABS Land berdiri pada tahun 2011 dengan project Puri Gading Residence 2 di Cimanggis, Depok. Sampai dengan tahun 2018 telah mengembangkan 8 ( delapan ) proyek di Jabodetabek dan Samarinda , Kalimantan Timur.</p>

        <p class="text-about">ABS Land terus berupaya untuk mempersembahkan produk perumahan dan komersil yang di rancang dan di bangun dengan pertimbangan yang matang agar dapat memberi kenyamanan dan nilai lebih bagi pengguna akhir ( enduser ).</p>

        <p class="text-about">ABS Land juga selalu mencari prospek baru untuk dikembangkan, baik membeli lahan ( landacquisition ) ataupun bekerja sama dengan pemiliklahan ( jointoperation / jointventure ). Dimana saat ini ekonomi sedang mengalami perlambatan, dan tidak lah mudah untuk menjual sebidang lahan dengan harga jual yang wajar. Kami percaya dengan pengalaman project yang telah diselesaikan dengan baik, ABS Land dapat memberi nilai tambah bagi calon mitra pemiliklahan.</p>
      </div>
      <div class="col-lg-4">
        <img src="<?= base_url('assets/images/about/about-image.png') ?>" class="img-fluid mt-4">
      </div>
    </div>
  </div>
</section>

<section id="corporateValue">
  <div class="jumbotron corporate-value-jumbotron mb-0 p-0">
    <div class="filter pb-4">
      <div class="container">
        <h1 class="text-center text-white title bold mb-4">CORPORATE VALUE</h1>
        <div class="row">
          <div class="col-lg-6">
            <div class="card cv-card m-3" data-aos="zoom-in">
              <div class="card-body">
                <h2>Innovation</h2>
                <p>To continuously thrive in providing innonative and quality products to our customers</p>
                <br>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="card cv-card m-3" data-aos="zoom-in">
              <div class="card-body">
                <h2>Integrity</h2>
                <p>To provide long-team and sustainable profiabilty for our partnes and stakeholders</p>
                <br>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="card cv-card m-3" data-aos="zoom-in">
              <div class="card-body">
                <h2>Contribution</h2>
                <p>To continously beneficially to the society and eviron-ment leading to a sustainable future</p>
                <br>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="card cv-card m-3" data-aos="zoom-in">
              <div class="card-body">
                <h2>Sustainable</h2>
                <p>To constinously maintain high level of integrity to provide long term mutually benefical relationship with prospective customers and partners</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="visionMission" class="py-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="vision">
          <h1 class="text-center title-blue bold mb-4">OUR VISION</h1>
          <div class="d-block mb-3">
            <img src="<?= base_url('assets/images/logo/1.png') ?>" class="mr-2" width="25">
            <span>To continuously maintain high to provide long term manually beneticial relationship with prospective customers and partners</span>
          </div>
        </div>

        <div class="vision">
          <h1 class="text-center title-blue bold mb-4">OUR MISSION</h1>
          <div class="d-block mb-3">
            <img src="<?= base_url('assets/images/logo/1.png') ?>" class="mr-2" width="25">
            <span>To focus and developing quality properties in fast growing areas across indonesia</span>
          </div>
          <div class="d-block mb-3">
            <img src="<?= base_url('assets/images/logo/1.png') ?>" class="mr-2" width="25">
            <span>To develop long term relationship with strategic partners with good corporate governance</span>
          </div>

          <div class="d-block mb-3">
            <img src="<?= base_url('assets/images/logo/1.png') ?>" class="mr-2" width="25">
            <span>To continuosly develop the quality of it's internal team and system to be continuosly in line with it's company values</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="project" class="py-5 bg-light">
  <div class="container">
    <h1 class="text-center bold">PROJECTS</h1>
    <div class="row">
      <div class="col-lg-12">
        <div class="float-md-left float-lg-left">
         <select id="cd-dropdown" class="cd-select">
          <option value="-1" selected>See All Project</option>
          <?php foreach (getProject() as $project): ?>
            <option value="1"><?= $project['project_name'] ?></option>
          <?php endforeach ?>
        </select>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="owl-carousel owl-theme mt-3">
        <?php foreach ($defaultProjectImages as $projectImage): ?>
          <div class="item">
            <img src="<?= base_url('assets/images/projects/' . $projectImage['image_name']) ?>">
          </div>
        <?php endforeach ?>
      </div>
    </div>
  </div>
</div>
</section>