<?php

function is_admin()
{
	$ci = get_instance();

	if ($ci->session->userdata('is_login') != true) {
		redirect('auth/login');
	}
}

function getProject()
{
	$ci = get_instance();

	return $ci->db->get('project')->result_array();
}

function getImage()
{
	$ci = get_instance();

	$get_image = $ci->db->get_where('admin', ['id_admin' => $ci->session->userdata('id_admin')])->row_array();

	if ($get_image) {
		if ($get_image['image'] != null) {
			return base_url() . "assets/images/users/" . $get_image['image'];
		} else {
			return base_url() . "assets/images/users/default/default.png";
		}
	} else {
		redirect('auth/logout');
	}
}

function getName()
{
	$ci = get_instance();

	$get_name = $ci->db->get_where('admin', ['id_admin' => $ci->session->userdata('id_admin')])->row_array();

	return $get_name['name'];
}

function filter($str)
{
	return htmlentities($str, ENT_QUOTES, 'UTF-8');
}

function cut($text, $length)
{
	$length = $length;
	$text = $text;

	return substr($text, 0, $length) . ' ...';
}

function page()
{
	$ci = get_instance();

	return $ci->uri->segment(2);
}