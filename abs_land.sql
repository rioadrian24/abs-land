-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2020 at 12:30 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `abs_land`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id_about` int(11) NOT NULL,
  `text_about` text NOT NULL,
  `image_about` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id_about`, `text_about`, `image_about`) VALUES
(1, 'PT. Artha Buana Samudera ( ABS Land ) adalah perusahaan yang bergerak di bidang pengembangan lahan untuk perumahan dan komersil. ABS Land berdiri pada tahun 2011 dengan project Puri Gading Residence 2 di Cimanggis, Depok. Sampai dengan tahun 2018 telah mengembangkan 8 ( delapan ) proyek di Jabodetabek dan Samarinda , Kalimantan Timur. <br><br>ABS Land terus berupaya untuk mempersembahkan produk perumahan dan komersil yang di rancang dan di bangun dengan pertimbangan yang matang agar dapat memberi kenyamanan dan nilai lebih bagi pengguna akhir ( enduser ). <br><br>ABS Land juga selalu mencari prospek baru untuk dikembangkan, baik membeli lahan ( landacquisition ) ataupun bekerja sama dengan pemiliklahan ( jointoperation / jointventure ). Dimana saat ini ekonomi sedang mengalami perlambatan, dan tidak lah mudah untuk menjual sebidang lahan dengan harga jual yang wajar. Kami percaya dengan pengalaman project yang telah diselesaikan dengan baik, ABS Land dapat memberi nilai tambah bagi calon mitra pemiliklahan.', 'default.png');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `date_update` varchar(11) NOT NULL DEFAULT '-'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `name`, `email`, `username`, `password`, `image`, `date_update`) VALUES
(1, 'Administrator', 'hello.absland@gmail.com', 'admin', '$2y$10$DK5iv0cpJargLvYZmU3ka.OTq9Lud6GeUj9vIGANdWtdCJ5MrJbMu', NULL, '1577945504');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_value`
--

CREATE TABLE `corporate_value` (
  `id_cv` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `corporate_value`
--

INSERT INTO `corporate_value` (`id_cv`, `title`, `description`) VALUES
(1, 'Innovation.', 'To continuously thrive in providing innonative and quality products to our customers'),
(2, 'Integrity.', 'To provide long-team and sustainable profiabilty for our partnes and stakeholders'),
(3, 'Contribution.', 'To continously beneficially to the society and eviron-ment leading to a sustainable future'),
(4, 'Sustainable.', 'To constinously maintain high level of integrity to provide long term mutually benefical relationship with prospective customers and partners');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id_message` int(11) NOT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `date` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mission`
--

CREATE TABLE `mission` (
  `id_mission` int(11) NOT NULL,
  `mission` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mission`
--

INSERT INTO `mission` (`id_mission`, `mission`) VALUES
(1, 'To focus and developing quality properties in fast growing areas across indonesia'),
(2, 'To develop long term relationship with strategic partners with good corporate governance'),
(3, 'To continuosly develop the quality of it''s internal team and system to be continuosly in line with it''s company values');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id_news` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `thumbnail` varchar(100) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `date` varchar(11) NOT NULL,
  `date_update` varchar(11) NOT NULL DEFAULT '-'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id_news`, `title`, `content`, `thumbnail`, `type`, `date`, `date_update`) VALUES
(1, 'OUR NEW LUXURY RESIDENCE', '<p xss=removed>Maecenas sed tincidunt tellus. Nam condimentum posuere commodo. Duis euismod turpis ut pulvinar aliquet. Suspendisse suscipit velit sem, vitae ultricies quam vestibulum id. Fusce et scelerisque tortor, a pretium ante. Duis sodales eleifend dolor vitae accumsan. Aenean non tincidunt quam, a luctus nulla. In porta elit lorem, sit amet tempus lacus blandit at. Nullam diam lorem, tempor nec turpis eget, varius tempor augue. Sed vel orci nulla. Sed posuere neque ut diam cursus cursus. Cras nibh erat, imperdiet quis consectetur nec, rhoncus nec magna. Nunc tempus dapibus molestie.</p><p xss=removed>Cras fermentum risus ex, eget sodales lectus bibendum vitae. Morbi nec nulla finibus, euismod tellus ut, accumsan arcu. Aenean hendrerit lacinia nulla, vitae lobortis mauris mattis quis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis lacinia, ipsum vitae hendrerit venenatis, mauris libero sagittis sem, et semper neque nibh vel turpis. Aenean sed maximus felis. Morbi sollicitudin mi a lectus rutrum, eget bibendum elit fermentum. Duis faucibus velit sit amet lorem tempus aliquet. Nullam porta vel urna in dapibus. Aliquam magna enim, hendrerit eu lorem id, fringilla dapibus urna. Praesent mattis augue elit, eget egestas mauris varius interdum. Maecenas sagittis est ac vulputate venenatis. Maecenas dolor leo, aliquet in mauris vel, facilisis rhoncus mauris.</p><p xss=removed>Fusce massa lorem, imperdiet a egestas et, vestibulum eget felis. Donec convallis iaculis tortor, malesuada sollicitudin erat. Suspendisse orci sapien, pretium ut pretium sed, finibus quis erat. Sed sed fermentum odio. Fusce ut tortor odio. Mauris congue vehicula gravida. Pellentesque et egestas eros. Suspendisse convallis libero ut neque fermentum, sit amet posuere est ultrices. Nullam et fringilla lectus. Curabitur eget odio vel magna pellentesque faucibus. Pellentesque fermentum mollis arcu nec blandit.</p><p xss=removed>Aliquam molestie venenatis leo sed laoreet. Aliquam facilisis ex erat, vitae placerat tortor placerat euismod. Etiam et ullamcorper ipsum, at mollis eros. Phasellus accumsan, nibh eget condimentum suscipit, massa velit facilisis ligula, at vestibulum justo mi consequat leo. Nulla orci sem, aliquam nec justo ac, lobortis tincidunt ante. Etiam viverra, odio vel suscipit egestas, massa neque varius dolor, ut vehicula mauris augue ut erat. Fusce a placerat diam. Nam sodales tincidunt erat, non mattis lectus hendrerit ut. Sed posuere tristique turpis.</p><p xss=removed>Morbi accumsan tristique neque, at iaculis tortor imperdiet at. Duis risus lorem, vulputate vel orci vel, ullamcorper pulvinar tellus. Fusce volutpat mollis turpis, a mattis velit egestas quis. Vivamus dignissim dui non nibh elementum, sit amet blandit nisi varius. Nullam eu nisi sit amet neque posuere faucibus auctor sit amet elit. In hac habitasse platea dictumst. Nulla vitae sem viverra, vehicula ex vel, tincidunt ex. Sed ante magna, pulvinar iaculis posuere et, varius et lectus. Ut leo diam, eleifend sit amet pulvinar eu, consequat non odio. Curabitur volutpat tristique ullamcorper. Duis aliquet ipsum ac arcu laoreet, quis scelerisque turpis imperdiet. Morbi id magna a sapien consequat ullamcorper eu quis ex. Quisque nisi mauris, vestibulum eu facilisis dignissim, molestie eget est. Suspendisse nec metus cursus, commodo orci in, scelerisque mauris.</p>', '360x2404.jpg', NULL, '1577586810', '1578485158'),
(3, 'OUR NEW LUXURY RESIDENCE', '<p xss=removed>Ut et laoreet mauris. Vestibulum sed dapibus leo. Etiam at lectus mi. Curabitur facilisis convallis blandit. Donec sit amet congue nibh. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam pretium ex ipsum, ut hendrerit ex dapibus at. Fusce facilisis lacus arcu, vel cursus enim efficitur ac. Nullam malesuada felis et justo tempus, sed lobortis lacus venenatis. Proin tristique neque id massa scelerisque aliquam. Cras ipsum ex, luctus eu posuere in, pharetra eu justo. Sed molestie id nisi ac pulvinar. Etiam ex purus, pulvinar vel sodales a, luctus id elit. Maecenas eu consectetur ex, id volutpat sem.</p><p xss=removed>Suspendisse scelerisque viverra elementum. Mauris faucibus id mauris quis sodales. Vivamus leo ex, malesuada eu libero ut, consectetur fringilla metus. Sed imperdiet diam non ultricies pulvinar. Vestibulum bibendum sagittis sem, vitae lobortis nunc fermentum vel. Duis egestas risus ac tortor dignissim tristique. Maecenas vel fermentum lacus, et rhoncus mauris. Duis tempor gravida pretium. Quisque placerat est vulputate, faucibus augue vitae, convallis libero. In vel suscipit nisl. Sed vestibulum ligula nisl, at lacinia quam egestas in. Nullam erat sem, dapibus sed venenatis sed, molestie at quam. Vivamus finibus ac velit non iaculis. Curabitur at iaculis augue. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas varius commodo tincidunt.</p><p xss=removed>Integer non dignissim orci. Integer scelerisque, nibh ac posuere dictum, nisi sem lacinia sem, ac porttitor tellus libero id felis. Aliquam commodo libero eu accumsan fermentum. Vivamus efficitur accumsan ex, a vehicula sapien ullamcorper eu. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent posuere malesuada erat, id eleifend sem dignissim nec. Vestibulum quis feugiat odio. Maecenas libero quam, fermentum ut eros nec, suscipit dictum purus. Integer dignissim consequat rhoncus. Quisque et tincidunt metus.</p><p xss=removed>Pellentesque quis lacinia quam, quis malesuada tortor. Donec vestibulum sodales consequat. Aliquam feugiat sem ac risus pharetra, eu maximus mauris tempus. Nam vehicula pulvinar facilisis. Praesent hendrerit velit nisl, nec porttitor sem pharetra at. Phasellus id venenatis risus, eget tempus nisi. Ut cursus nulla quis tellus porttitor, et semper sapien efficitur. Aenean quis rhoncus elit. Phasellus odio elit, vestibulum ut ante eu, pharetra pellentesque dui. Aliquam a erat et diam feugiat tempus eu vel arcu. Integer interdum tristique ipsum in hendrerit. Vivamus justo tellus, vestibulum eget viverra blandit, tincidunt vel ante. Etiam a lectus eget libero rhoncus consequat id vel enim. Proin facilisis magna risus, eget porta nunc egestas sit amet. Vivamus sit amet gravida ante, eget pharetra sapien. Duis commodo enim sit amet eros accumsan, ac aliquet ante rutrum.</p><p xss=removed>Donec nec felis vel neque fermentum iaculis vitae eget lorem. Maecenas posuere hendrerit magna in mattis. Phasellus placerat mauris sem, at placerat elit elementum a. Praesent sollicitudin sapien sed quam mattis, vitae egestas metus congue. Duis condimentum erat rutrum bibendum pellentesque. In tortor odio, tincidunt vel enim aliquet, rhoncus congue elit. In leo felis, feugiat eu tempus eget, ullamcorper a sem. Nunc massa justo, pharetra ac lobortis at, pretium ac augue. Suspendisse mollis justo ac congue luctus. Morbi malesuada eros eu ligula accumsan, quis condimentum sem tincidunt. Duis ornare nibh ut eros auctor, non aliquam risus accumsan. Aenean et dolor nisi. Pellentesque at lorem quis elit vehicula rutrum. Donec quis magna arcu. Curabitur eget ante molestie, maximus metus nec, tincidunt ipsum.</p>', '360x2403.jpg', NULL, '1577594220', '1578485137'),
(5, 'OUR NEW LUXURY RESIDENCE', '<p xss=removed>Sed at pellentesque dolor. Aenean enim ligula, sodales quis interdum tristique, accumsan eu dui. Fusce sed mi convallis, luctus massa eu, dictum dui. Sed sit amet quam sem. Maecenas tincidunt ex quis massa maximus, ac tincidunt ligula laoreet. Mauris vel turpis et diam tristique ultricies. Etiam vitae turpis iaculis, vestibulum magna ac, dignissim mi. Fusce at sem sit amet felis lobortis aliquam id vel dui. Etiam iaculis, mi ac tempus rhoncus, enim est molestie massa, vitae efficitur erat mauris id velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas eget vulputate nisi. Sed quis condimentum erat. Fusce vulputate risus a luctus tempus.</p><p xss=removed>Phasellus tincidunt enim at tempus dictum. Cras in consectetur ex, id vulputate mauris. Vivamus id lacus a arcu auctor vestibulum. Nullam molestie a diam condimentum aliquet. Cras egestas mauris tellus, a accumsan lacus vestibulum a. Proin mollis egestas mauris, sed feugiat nunc scelerisque eget. Sed posuere massa non faucibus ornare. Quisque nibh tellus, rutrum sit amet aliquet eget, feugiat eu dui. Vivamus tristique leo a nisl fringilla, eu rhoncus nibh porttitor. Etiam iaculis dolor ac massa pulvinar fringilla.</p><p xss=removed>Nunc lacinia, ex eu tristique vestibulum, enim augue imperdiet nibh, non fermentum nisl nulla sit amet ante. In hac habitasse platea dictumst. Proin eu blandit justo. Pellentesque sit amet maximus felis. Nulla efficitur sem eu magna porttitor rutrum. Morbi ultrices vel ipsum ac malesuada. Quisque laoreet dapibus enim id tristique. Vivamus dapibus accumsan magna, vel rutrum eros volutpat nec. Maecenas semper placerat iaculis.</p><p xss=removed>Integer tempus sollicitudin nulla, sit amet elementum urna mollis a. Integer varius sapien in est bibendum sollicitudin. Cras quis ullamcorper mauris, quis egestas felis. Vestibulum maximus sapien odio, in vehicula odio pellentesque sed. Donec ultricies sem quis scelerisque faucibus. Praesent maximus metus volutpat, sodales libero et, tincidunt odio. Pellentesque et neque pretium, egestas eros sit amet, malesuada ligula.</p><p xss=removed>In elementum pretium nibh, vel tempor dui placerat in. Proin consequat, sem non aliquam suscipit, erat turpis interdum tortor, et auctor nulla magna vitae nisi. Maecenas efficitur, orci posuere ornare pharetra, leo libero auctor nisl, feugiat tristique diam justo nec enim. Aenean consectetur eget leo sit amet laoreet. Nullam cursus est sed arcu bibendum, ut suscipit lorem commodo. In et malesuada elit. Suspendisse vehicula, libero a rutrum pulvinar, elit risus pretium ligula, sit amet volutpat justo tellus at tortor. Fusce sit amet lobortis urna. Donec porta tellus vitae lectus sodales, id lobortis mi auctor. Quisque dolor enim, posuere in volutpat vel, gravida eu sem. Aenean placerat urna ut justo feugiat consequat.</p>', '360x2402.jpg', NULL, '1577628473', '1578485116'),
(6, 'OUR NEW LUXURY RESIDENCE', '<p xss=removed>Phasellus justo libero, semper ut elit non, mollis ornare mi. Nulla accumsan mi tellus, id pretium velit dignissim vel. Curabitur venenatis est felis, id porttitor ante sollicitudin at. Ut pretium, enim in venenatis pellentesque, tortor diam facilisis sem, nec cursus quam velit ac odio. Donec fermentum tempus purus, nec luctus leo porta et. Pellentesque ut pretium lectus. Vestibulum nec odio felis. Nulla bibendum nulla massa, vel auctor eros tincidunt vel. Suspendisse id eros purus.</p><p xss=removed>Ut commodo, dui et gravida scelerisque, turpis lectus luctus sem, non convallis nisi mi eu diam. Duis purus ligula, viverra ac ex sed, accumsan blandit elit. Aliquam et tellus a lacus luctus sodales in in turpis. Maecenas ante tortor, venenatis in lorem eget, rhoncus lacinia orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed pretium lobortis ipsum sollicitudin tempus. Quisque eget cursus erat. Pellentesque lobortis dolor sed felis hendrerit, et laoreet sem dapibus. Phasellus faucibus posuere nisi, at tincidunt dui placerat in. In egestas blandit nisl, eu tincidunt ante gravida non. Quisque hendrerit nunc sed leo porttitor, et luctus dolor elementum. Maecenas suscipit sed dolor sed sollicitudin. Pellentesque pretium faucibus ex at mattis.</p><p xss=removed>Morbi faucibus sed libero eu fermentum. Fusce porta congue lectus, at laoreet augue consequat et. Nulla facilisi. Pellentesque dictum pretium turpis. Etiam commodo finibus auctor. Nunc et pulvinar leo. Aenean arcu felis, auctor ac odio vel, euismod suscipit lectus. Mauris in tristique sem. Integer id neque et lacus varius mollis vel eget ligula. Sed pellentesque arcu et nulla gravida commodo. Ut hendrerit, tellus a venenatis sollicitudin, erat orci lobortis nulla, at fermentum risus urna at enim. Vestibulum feugiat ligula a interdum blandit. Nullam et metus nec enim consequat rutrum in sit amet elit. Duis quis felis augue. Vestibulum fermentum nulla a elementum eleifend. Phasellus vitae ligula id nibh tincidunt dignissim ut nec metus.</p><p xss=removed>Aenean ultricies sit amet augue ac ultrices. Curabitur a nisl nec ipsum faucibus imperdiet. Vestibulum ac lorem libero. Nam dapibus tempus sapien, et interdum nunc congue vitae. Suspendisse aliquam, erat et euismod fringilla, neque ipsum ultrices ligula, pellentesque imperdiet quam magna sit amet nisl. Maecenas dolor nulla, interdum vel auctor eget, sodales in diam. Curabitur mollis fermentum orci, in dictum elit commodo luctus. Ut eget ipsum imperdiet, varius mi non, efficitur nunc. Pellentesque volutpat viverra nibh, nec lacinia elit gravida sed. Phasellus pharetra diam arcu, sit amet auctor magna commodo id. Aenean rutrum in nisl at posuere. Cras eros purus, luctus eu odio quis, dictum ultrices diam. Nam rutrum eu eros vel pulvinar. Donec nec urna tortor. Nullam sed sollicitudin sapien, venenatis sollicitudin sapien.</p><p xss=removed>Etiam ultrices, felis nec ornare lobortis, augue eros placerat lorem, sit amet mattis risus leo quis massa. Nam felis nunc, commodo id ligula sed, pulvinar pretium velit. Morbi ullamcorper ac ante eu sodales. Donec dignissim laoreet maximus. Ut convallis, leo nec tristique consectetur, dolor velit volutpat diam, iaculis feugiat eros ligula bibendum eros. Nam accumsan ornare ornare. Fusce molestie sem ipsum, a lobortis elit mollis finibus. Curabitur facilisis, sem dictum fringilla interdum, elit nisl cursus justo, ac dapibus risus quam at leo. Duis sit amet mi libero. In sed urna eget arcu faucibus semper sit amet id nunc. Nullam tincidunt, nisi non fermentum fringilla, turpis metus vehicula mauris, quis maximus massa odio eu velit. Vestibulum varius leo eu tristique sollicitudin.</p>', '360x2401.jpg', NULL, '1577634096', '1578485091'),
(7, 'OUR NEW LUXURY RESIDENCE', '<p xss=removed>Donec eget est in nisl dignissim porttitor. Duis ut ipsum nec metus fermentum porta. Etiam vel finibus est. In lacinia sollicitudin arcu in sodales. Aenean euismod tincidunt ultricies. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque quis faucibus tellus, eu pharetra dolor. Integer mollis mi vitae euismod sagittis. Sed dui lacus, cursus mollis turpis eu, condimentum iaculis quam. Vestibulum eget ex nec dui congue tristique eget nec lorem. In hac habitasse platea dictumst.</p><p xss=removed>Vivamus sit amet tempor lectus, non malesuada dui. Nunc iaculis nunc quis porta sodales. Morbi auctor molestie ligula tincidunt volutpat. Quisque ornare tellus eget elit fermentum fringilla. Quisque gravida viverra placerat. Nulla finibus massa eu erat rhoncus sollicitudin. Ut egestas risus in suscipit ullamcorper. Praesent eu lorem quam. Maecenas metus ex, porttitor id sagittis blandit, faucibus sit amet purus. Nulla nec orci vitae mi imperdiet vehicula ut sit amet diam. Nulla hendrerit turpis et dui ullamcorper vulputate. Aenean blandit dolor a elit eleifend, vel convallis neque aliquet. Ut finibus vulputate fringilla. Aenean quis purus blandit, sollicitudin augue tempus, rhoncus arcu.</p><p xss=removed>Donec et gravida erat. Suspendisse potenti. Aenean tempus vulputate magna et luctus. Duis ornare nisl eget maximus convallis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed purus diam, porttitor ut magna quis, aliquam imperdiet eros. Vestibulum finibus, purus id lobortis mollis, ipsum sapien lobortis mauris, vitae sollicitudin est libero ac augue. Cras ultricies felis leo, non ornare purus auctor eu. Phasellus dictum odio elit, non tristique mauris lacinia vitae. Cras tincidunt ac dolor in tincidunt.</p><p xss=removed>Proin a venenatis odio. Aenean dignissim sodales finibus. Vestibulum laoreet congue faucibus. Nam nec porta arcu. Mauris blandit velit id euismod tempor. Integer efficitur pulvinar fermentum. Fusce tempus massa sit amet ante consectetur, ut commodo magna porttitor. Praesent vulputate sed quam quis egestas. Integer viverra quis odio et posuere. Praesent in lacus risus. Vivamus laoreet convallis massa, vitae ullamcorper ligula suscipit vel. Donec eu augue non lacus varius blandit in in nisl. Nullam fermentum, orci eget porttitor tempus, elit enim ultrices magna, nec laoreet tortor ligula sed ex. Vivamus in justo iaculis, tempus dui lacinia, scelerisque sapien.</p><p xss=removed>Sed non semper dui. Cras ac neque venenatis, hendrerit erat ac, iaculis ipsum. Cras elementum orci vitae libero gravida, eget tincidunt quam porttitor. Maecenas euismod erat non placerat facilisis. Curabitur tempus commodo purus sit amet vehicula. Pellentesque porta, purus et scelerisque vehicula, ligula orci ullamcorper ante, et posuere dui turpis at urna. Ut leo diam, bibendum et mattis vel, tincidunt ac justo. Proin porttitor, nulla in scelerisque tincidunt, nulla massa rhoncus turpis, ut pulvinar diam mauris vel mauris. Ut et arcu dolor. Sed vitae turpis ut nulla scelerisque pellentesque id et velit. Morbi vehicula ante sed urna ullamcorper egestas. Nam ut placerat nisl, nec suscipit lorem. Sed aliquam volutpat feugiat.</p>', '360x240.jpg', NULL, '1577875881', '1578485064'),
(8, 'OUR NEW LUXURY RESIDENCE', '<p xss=removed>Aliquam convallis neque vitae sagittis molestie. Aliquam condimentum velit eget dui aliquet euismod. Maecenas vel enim nec diam blandit condimentum. Integer quis elit tincidunt, consequat massa ut, pulvinar nulla. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tellus sapien, rhoncus a odio nec, bibendum feugiat elit. Proin dictum nulla risus, eget ultrices diam consequat id. Aenean ex diam, interdum et neque vitae, sagittis hendrerit risus. Fusce sodales nunc et ligula aliquam, in sagittis risus rhoncus.</p><p xss=removed>Morbi tristique suscipit tincidunt. Curabitur consequat mi non felis sodales porta in ac diam. Nam auctor, lacus non faucibus dictum, felis sapien condimentum quam, mattis eleifend lectus odio vitae nunc. Phasellus sed tortor mollis, venenatis quam vel, laoreet lorem. Vivamus ipsum mauris, malesuada non ante ac, convallis facilisis nisl. Fusce lectus purus, malesuada eget malesuada et, auctor in lorem. Vestibulum nibh urna, elementum vel pretium at, cursus eu sem. Cras mollis euismod sem, accumsan rutrum erat suscipit eget. Integer nec rhoncus libero. In hac habitasse platea dictumst. Donec turpis mi, euismod tincidunt magna ut, lacinia posuere elit. Etiam ut ornare nisi. Integer auctor, enim ac consectetur finibus, lacus tortor bibendum lacus, in tempor diam lorem vitae turpis. Curabitur in aliquet nisi. Praesent hendrerit lobortis nibh, vitae faucibus velit tempor quis. Duis quis diam arcu.</p><p xss=removed>Cras id efficitur ipsum. Praesent et purus sollicitudin, vulputate sapien nec, pretium nulla. Donec lobortis dui leo, eu accumsan ligula vulputate id. Nullam pellentesque risus diam, non sagittis lorem dignissim et. Aenean in sagittis diam. Nullam interdum, sem vel viverra auctor, purus metus tincidunt nisl, vitae egestas elit dolor sed magna. In auctor ante rutrum lacus semper, ac congue enim varius. Vivamus dapibus in diam sit amet ullamcorper. Nulla id metus libero. Aenean cursus nunc quis suscipit luctus. Phasellus pellentesque ligula tortor, quis imperdiet nisi tristique id. Donec sed massa ipsum. Vivamus eu scelerisque leo, vel lacinia risus.</p><p xss=removed>Sed dignissim eros in pulvinar molestie. Proin sodales convallis semper. Maecenas non dapibus tellus. Curabitur elementum diam sed nisl convallis, eu vulputate enim vehicula. In quis lacinia eros, ac finibus dolor. Nunc dapibus congue lectus, at eleifend sapien rutrum in. Sed egestas egestas mi quis congue. Fusce porta a metus id porta. Etiam elementum convallis egestas. Proin diam quam, luctus ut velit non, auctor iaculis leo. Donec sed nisl in neque hendrerit mollis nec id ipsum. Curabitur nec justo ipsum. Proin facilisis consequat pharetra.</p><p xss=removed>Aliquam est ante, aliquet eget odio sit amet, vestibulum vulputate lorem. Proin ligula metus, commodo eget neque sit amet, mattis tempus mi. Nulla a tempor libero. Sed vitae neque nec ex molestie laoreet. Integer nibh metus, varius vel accumsan non, suscipit a risus. Nullam nec enim diam. Etiam quis libero non neque semper pulvinar sit amet at augue. Aliquam ut vestibulum massa. Mauris a orci euismod, tincidunt odio quis, scelerisque metus. Pellentesque vehicula purus augue, ut convallis mi maximus sed.</p>', '850x350.jpg', 'featured', '1577875892', '1578485033'),
(9, 'OUR NEW LUXURY RESIDENCE', '<p xss="removed">Aliquam convallis neque vitae sagittis molestie. Aliquam condimentum velit eget dui aliquet euismod. Maecenas vel enim nec diam blandit condimentum. Integer quis elit tincidunt, consequat massa ut, pulvinar nulla. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tellus sapien, rhoncus a odio nec, bibendum feugiat elit. Proin dictum nulla risus, eget ultrices diam consequat id. Aenean ex diam, interdum et neque vitae, sagittis hendrerit risus. Fusce sodales nunc et ligula aliquam, in sagittis risus rhoncus.</p><p xss="removed">Morbi tristique suscipit tincidunt. Curabitur consequat mi non felis sodales porta in ac diam. Nam auctor, lacus non faucibus dictum, felis sapien condimentum quam, mattis eleifend lectus odio vitae nunc. Phasellus sed tortor mollis, venenatis quam vel, laoreet lorem. Vivamus ipsum mauris, malesuada non ante ac, convallis facilisis nisl. Fusce lectus purus, malesuada eget malesuada et, auctor in lorem. Vestibulum nibh urna, elementum vel pretium at, cursus eu sem. Cras mollis euismod sem, accumsan rutrum erat suscipit eget. Integer nec rhoncus libero. In hac habitasse platea dictumst. Donec turpis mi, euismod tincidunt magna ut, lacinia posuere elit. Etiam ut ornare nisi. Integer auctor, enim ac consectetur finibus, lacus tortor bibendum lacus, in tempor diam lorem vitae turpis. Curabitur in aliquet nisi. Praesent hendrerit lobortis nibh, vitae faucibus velit tempor quis. Duis quis diam arcu.</p><p xss="removed">Cras id efficitur ipsum. Praesent et purus sollicitudin, vulputate sapien nec, pretium nulla. Donec lobortis dui leo, eu accumsan ligula vulputate id. Nullam pellentesque risus diam, non sagittis lorem dignissim et. Aenean in sagittis diam. Nullam interdum, sem vel viverra auctor, purus metus tincidunt nisl, vitae egestas elit dolor sed magna. In auctor ante rutrum lacus semper, ac congue enim varius. Vivamus dapibus in diam sit amet ullamcorper. Nulla id metus libero. Aenean cursus nunc quis suscipit luctus. Phasellus pellentesque ligula tortor, quis imperdiet nisi tristique id. Donec sed massa ipsum. Vivamus eu scelerisque leo, vel lacinia risus.</p><p xss="removed">Sed dignissim eros in pulvinar molestie. Proin sodales convallis semper. Maecenas non dapibus tellus. Curabitur elementum diam sed nisl convallis, eu vulputate enim vehicula. In quis lacinia eros, ac finibus dolor. Nunc dapibus congue lectus, at eleifend sapien rutrum in. Sed egestas egestas mi quis congue. Fusce porta a metus id porta. Etiam elementum convallis egestas. Proin diam quam, luctus ut velit non, auctor iaculis leo. Donec sed nisl in neque hendrerit mollis nec id ipsum. Curabitur nec justo ipsum. Proin facilisis consequat pharetra.</p><p xss="removed">Aliquam est ante, aliquet eget odio sit amet, vestibulum vulputate lorem. Proin ligula metus, commodo eget neque sit amet, mattis tempus mi. Nulla a tempor libero. Sed vitae neque nec ex molestie laoreet. Integer nibh metus, varius vel accumsan non, suscipit a risus. Nullam nec enim diam. Etiam quis libero non neque semper pulvinar sit amet at augue. Aliquam ut vestibulum massa. Mauris a orci euismod, tincidunt odio quis, scelerisque metus. Pellentesque vehicula purus augue, ut convallis mi maximus sed.</p>', '360x2405.jpg', NULL, '1578523546', '-');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id_project` int(11) NOT NULL,
  `banner` varchar(100) DEFAULT NULL,
  `project_name` varchar(100) NOT NULL,
  `project_description` text NOT NULL,
  `gmaps_link` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id_project`, `banner`, `project_name`, `project_description`, `gmaps_link`) VALUES
(1, NULL, 'Ananda Residence', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias voluptate consequatur dolorem nisi cum blanditiis, quisquam deleniti incidunt vero sunt fuga, nulla nihil autem aliquam? Excepturi impedit, omnis perspiciatis! Placeat ut praesentium quae reprehenderit esse vitae in asperiores, eligendi aperiam accusamus delectus consequatur ratione non nulla quos officia, amet aut atque neque doloremque similique quod facere incidunt explicabo eos! Quibusdam qui, laboriosam quaerat dicta consectetur quia facilis cum fugiat minima labore nemo assumenda illum aliquid temporibus enim velit suscipit quis, cupiditate dolores sunt nobis. Fugiat accusamus magni, a quisquam, assumenda molestias dolor beatae quis doloribus, pariatur nesciunt tempora rem laboriosam?<br></p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15867.622384397751!2d106.7765362520909!3d-6.143381595717622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d904b1f2d13%3A0x6788424cc38b1033!2sJl.%20Pluit%20Barat%20I%20No.51%2C%20RT.23%2FRW.8%2C%20Pluit%2C%20Kec.%20Penjaringan%2C%20Kota%20Jkt%20Utara%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2014450!5e0!3m2!1sen!2sid!4v1570434517241!5m2!1sen!2sid'),
(2, NULL, 'Ananda Townhouse', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias voluptate consequatur dolorem nisi cum blanditiis, quisquam deleniti incidunt vero sunt fuga, nulla nihil autem aliquam? Excepturi impedit, omnis perspiciatis! Placeat ut praesentium quae reprehenderit esse vitae in asperiores, eligendi aperiam accusamus delectus consequatur ratione non nulla quos officia, amet aut atque neque doloremque similique quod facere incidunt explicabo eos! Quibusdam qui, laboriosam quaerat dicta consectetur quia facilis cum fugiat minima labore nemo assumenda illum aliquid temporibus enim velit suscipit quis, cupiditate dolores sunt nobis. Fugiat accusamus magni, a quisquam, assumenda molestias dolor beatae quis doloribus, pariatur nesciunt tempora rem laboriosam?<br></p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15867.622384397751!2d106.7765362520909!3d-6.143381595717622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d904b1f2d13%3A0x6788424cc38b1033!2sJl.%20Pluit%20Barat%20I%20No.51%2C%20RT.23%2FRW.8%2C%20Pluit%2C%20Kec.%20Penjaringan%2C%20Kota%20Jkt%20Utara%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2014450!5e0!3m2!1sen!2sid!4v1570434517241!5m2!1sen!2sid'),
(3, NULL, 'Bintaro Central', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15867.622384397751!2d106.7765362520909!3d-6.143381595717622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d904b1f2d13%3A0x6788424cc38b1033!2sJl.%20Pluit%20Barat%20I%20No.51%2C%20RT.23%2FRW.8%2C%20Pluit%2C%20Kec.%20Penjaringan%2C%20Kota%20Jkt%20Utara%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2014450!5e0!3m2!1sen!2sid!4v1570434517241!5m2!1sen!2sid<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias voluptate consequatur dolorem nisi cum blanditiis, quisquam deleniti incidunt vero sunt fuga, nulla nihil autem aliquam? Excepturi impedit, omnis perspiciatis! Placeat ut praesentium quae reprehenderit esse vitae in asperiores, eligendi aperiam accusamus delectus consequatur ratione non nulla quos officia, amet aut atque neque doloremque similique quod facere incidunt explicabo eos! Quibusdam qui, laboriosam quaerat dicta consectetur quia facilis cum fugiat minima labore nemo assumenda illum aliquid temporibus enim velit suscipit quis, cupiditate dolores sunt nobis. Fugiat accusamus magni, a quisquam, assumenda molestias dolor beatae quis doloribus, pariatur nesciunt tempora rem laboriosam?<br></p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15867.622384397751!2d106.7765362520909!3d-6.143381595717622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d904b1f2d13%3A0x6788424cc38b1033!2sJl.%20Pluit%20Barat%20I%20No.51%2C%20RT.23%2FRW.8%2C%20Pluit%2C%20Kec.%20Penjaringan%2C%20Kota%20Jkt%20Utara%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2014450!5e0!3m2!1sen!2sid!4v1570434517241!5m2!1sen!2sid'),
(4, NULL, 'Sawangan Central', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias voluptate consequatur dolorem nisi cum blanditiis, quisquam deleniti incidunt vero sunt fuga, nulla nihil autem aliquam? Excepturi impedit, omnis perspiciatis! Placeat ut praesentium quae reprehenderit esse vitae in asperiores, eligendi aperiam accusamus delectus consequatur ratione non nulla quos officia, amet aut atque neque doloremque similique quod facere incidunt explicabo eos! Quibusdam qui, laboriosam quaerat dicta consectetur quia facilis cum fugiat minima labore nemo assumenda illum aliquid temporibus enim velit suscipit quis, cupiditate dolores sunt nobis. Fugiat accusamus magni, a quisquam, assumenda molestias dolor beatae quis doloribus, pariatur nesciunt tempora rem laboriosam?<br></p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15867.622384397751!2d106.7765362520909!3d-6.143381595717622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d904b1f2d13%3A0x6788424cc38b1033!2sJl.%20Pluit%20Barat%20I%20No.51%2C%20RT.23%2FRW.8%2C%20Pluit%2C%20Kec.%20Penjaringan%2C%20Kota%20Jkt%20Utara%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2014450!5e0!3m2!1sen!2sid!4v1570434517241!5m2!1sen!2sid'),
(5, NULL, 'Inhutani Green Residence', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias voluptate consequatur dolorem nisi cum blanditiis, quisquam deleniti incidunt vero sunt fuga, nulla nihil autem aliquam? Excepturi impedit, omnis perspiciatis! Placeat ut praesentium quae reprehenderit esse vitae in asperiores, eligendi aperiam accusamus delectus consequatur ratione non nulla quos officia, amet aut atque neque doloremque similique quod facere incidunt explicabo eos! Quibusdam qui, laboriosam quaerat dicta consectetur quia facilis cum fugiat minima labore nemo assumenda illum aliquid temporibus enim velit suscipit quis, cupiditate dolores sunt nobis. Fugiat accusamus magni, a quisquam, assumenda molestias dolor beatae quis doloribus, pariatur nesciunt tempora rem laboriosam?<br></p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15867.622384397751!2d106.7765362520909!3d-6.143381595717622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d904b1f2d13%3A0x6788424cc38b1033!2sJl.%20Pluit%20Barat%20I%20No.51%2C%20RT.23%2FRW.8%2C%20Pluit%2C%20Kec.%20Penjaringan%2C%20Kota%20Jkt%20Utara%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2014450!5e0!3m2!1sen!2sid!4v1570434517241!5m2!1sen!2sid'),
(6, NULL, 'Puri Gading Residences 2', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias voluptate consequatur dolorem nisi cum blanditiis, quisquam deleniti incidunt vero sunt fuga, nulla nihil autem aliquam? Excepturi impedit, omnis perspiciatis! Placeat ut praesentium quae reprehenderit esse vitae in asperiores, eligendi aperiam accusamus delectus consequatur ratione non nulla quos officia, amet aut atque neque doloremque similique quod facere incidunt explicabo eos! Quibusdam qui, laboriosam quaerat dicta consectetur quia facilis cum fugiat minima labore nemo assumenda illum aliquid temporibus enim velit suscipit quis, cupiditate dolores sunt nobis. Fugiat accusamus magni, a quisquam, assumenda molestias dolor beatae quis doloribus, pariatur nesciunt tempora rem laboriosam?<br></p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15867.622384397751!2d106.7765362520909!3d-6.143381595717622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d904b1f2d13%3A0x6788424cc38b1033!2sJl.%20Pluit%20Barat%20I%20No.51%2C%20RT.23%2FRW.8%2C%20Pluit%2C%20Kec.%20Penjaringan%2C%20Kota%20Jkt%20Utara%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2014450!5e0!3m2!1sen!2sid!4v1570434517241!5m2!1sen!2sid'),
(7, NULL, 'Tes', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias voluptate consequatur dolorem nisi cum blanditiis, quisquam deleniti incidunt vero sunt fuga, nulla nihil autem aliquam? Excepturi impedit, omnis perspiciatis! Placeat ut praesentium quae reprehenderit esse vitae in asperiores, eligendi aperiam accusamus delectus consequatur ratione non nulla quos officia, amet aut atque neque doloremque similique quod facere incidunt explicabo eos! Quibusdam qui, laboriosam quaerat dicta consectetur quia facilis cum fugiat minima labore nemo assumenda illum aliquid temporibus enim velit suscipit quis, cupiditate dolores sunt nobis. Fugiat accusamus magni, a quisquam, assumenda molestias dolor beatae quis doloribus, pariatur nesciunt tempora rem laboriosam?<br></p>', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15867.622384397751!2d106.7765362520909!3d-6.143381595717622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d904b1f2d13%3A0x6788424cc38b1033!2sJl.%20Pluit%20Barat%20I%20No.51%2C%20RT.23%2FRW.8%2C%20Pluit%2C%20Kec.%20Penjaringan%2C%20Kota%20Jkt%20Utara%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2014450!5e0!3m2!1sen!2sid!4v1570434517241!5m2!1sen!2sid');

-- --------------------------------------------------------

--
-- Table structure for table `project_image`
--

CREATE TABLE `project_image` (
  `id_project_image` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `image_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vision`
--

CREATE TABLE `vision` (
  `id_vision` int(11) NOT NULL,
  `vision` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vision`
--

INSERT INTO `vision` (`id_vision`, `vision`) VALUES
(1, 'To continuously maintain high to provide long term manually beneticial relationship with prospective customers and partners');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id_about`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `corporate_value`
--
ALTER TABLE `corporate_value`
  ADD PRIMARY KEY (`id_cv`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id_message`);

--
-- Indexes for table `mission`
--
ALTER TABLE `mission`
  ADD PRIMARY KEY (`id_mission`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id_news`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id_project`);

--
-- Indexes for table `project_image`
--
ALTER TABLE `project_image`
  ADD PRIMARY KEY (`id_project_image`);

--
-- Indexes for table `vision`
--
ALTER TABLE `vision`
  ADD PRIMARY KEY (`id_vision`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id_about` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `corporate_value`
--
ALTER TABLE `corporate_value`
  MODIFY `id_cv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id_message` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `mission`
--
ALTER TABLE `mission`
  MODIFY `id_mission` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id_news` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id_project` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `project_image`
--
ALTER TABLE `project_image`
  MODIFY `id_project_image` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `vision`
--
ALTER TABLE `vision`
  MODIFY `id_vision` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
